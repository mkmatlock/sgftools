import numpy as np

class UnionFind(object):
    def __init__(self, shape):
        self.num_weights = {}
        self.parent_pointers = {}
        self.num_to_objects = {}
        self.objects_to_num = {}
        self._insert_objects(shape)

    def _insert_objects(self, shape):
        for i in range(shape[0]):
            for j in range(shape[1]):
                obj = (i,j)
                obj_num = len(self.objects_to_num)+1
                self.num_weights[obj_num] = 1
                self.objects_to_num[obj] = obj_num
                self.num_to_objects[obj_num] = obj
                self.parent_pointers[obj_num] = obj_num

    def find(self, obj):
        stk = [self.objects_to_num[obj]]
        par = self.parent_pointers[stk[-1]]
        while par != stk[-1]:
            stk.append(par)
            par = self.parent_pointers[par]
        for i in stk:
            self.parent_pointers[i] = par
        return par

    def union(self, obj1, obj2):
        o1p = self.num_to_objects[self.find(obj1)]
        o2p = self.num_to_objects[self.find(obj2)]
        if o1p != o2p:
            on1 = self.objects_to_num[o1p]
            on2 = self.objects_to_num[o2p]
            w1 = self.num_weights[on1]
            w2 = self.num_weights[on2]
            if w1 < w2:
                o1p, o2p, on1, on2, w1, w2 = o2p, o1p, on2, on1, w2, w1
            self.num_weights[on1] = w1+w2
            del self.num_weights[on2]
            self.parent_pointers[on2] = on1

def uniq(L):
    seen = set()
    M = []
    for i in L:
        if i not in seen:
            seen.add(i)
            M.append(i)
    return M

def is_first_line(coords, size):
    return np.all((coords[:,0]==0) | (coords[:,0]==size-1) | (coords[:,1]==0) | (coords[:,1]==size-1))

def neighbors(i, j, shape):
    if i>0:
        yield i-1, j
    if i<shape[0]-1:
        yield i+1, j
    if j>0:
        yield i, j-1
    if j<shape[1]-1:
        yield i, j+1

def convert_to_pos(idx):
    coords = 'abcdefghijklmnopqrst'
    return coords[idx[0]] + coords[idx[1]]

def group_liberties(position, group):
    for i, j in group:
        for i2, j2 in neighbors(i, j, position.shape):
            if position[i2, j2] == 0:
                yield i2, j2

def group_neighbors(position, groups, gnum):
    indices = np.argwhere(groups==gnum)
    seen = set()
    for i, j in indices:
        for i2, j2 in neighbors(i, j, position.shape):
            g = groups[i2, j2]
            if g > 0 and g != groups[i,j] and g not in seen:
                yield g
                seen.add(g)

class Goban(object):
    def __init__(self, position=None):
        if position is not None:
            self.position = position.copy()
            self.groups, self.colors = self._find_groups()
            self.liberties = self._find_liberties()

    def _copy(self):
        G = Goban()
        G.position = self.position.copy()
        G.groups = self.groups.copy()
        G.colors = self.colors.copy()
        G.liberties = self.liberties.copy()
        return G

    def _find_liberties(self):
        position = self.position
        groups = self.groups
        liberties = {}

        for gnum in np.unique(groups):
            if gnum==0: continue
            lib_positions = np.zeros(position.shape, dtype=np.int)
            query = groups==gnum
            stones = np.argwhere(query)
            for i, j in group_liberties(position, stones):
                lib_positions[i,j] = 1
            liberties[gnum] = lib_positions
        return liberties

    def _find_groups(self):
        position = self.position
        ufind = UnionFind(position.shape)
        for i in range(position.shape[0]):
            for j in range(position.shape[1]):
                color = position[i,j]
                if color==0: continue
                if i<position.shape[0]-1 and position[i+1,j]==color:
                    ufind.union((i,j), (i+1,j))
                if j<position.shape[1]-1 and position[i,j+1]==color:
                    ufind.union((i,j), (i,j+1))

        groups = np.zeros(position.shape, dtype=np.int)
        colors = {}
        for i in range(position.shape[0]):
            for j in range(position.shape[1]):
                if position[i,j]>0:
                    gnum = ufind.find((i,j))
                    groups[i,j] = gnum
                    colors[gnum] = position[i,j]
        return groups, colors

    def play_move(self, i, j, c):
        G = self._copy()
        position = G.position
        shape = position.shape
        groups = G.groups
        colors = G.colors
        liberties = G.liberties

        position[i,j] = c
        new_group = np.max(groups)+1
        groups[i,j] = new_group
        liberties[new_group] = np.zeros(shape, dtype=np.int)
        for idx in neighbors(i,j,shape):
            liberties[new_group][idx] = 1 if position[idx]==0 else 0
            colors[new_group] = c

        adjacent = [ idx for idx in neighbors(i,j,shape) if groups[idx]>0 ]
        for idx in adjacent:
            g = groups[idx]
            liberties[g] = liberties[g].copy()
            liberties[g][i,j]-=1

        merge = [groups[idx] for idx in adjacent if position[idx]==c] + [new_group]
        merge = uniq(merge)
        merge_group = min(merge)

        liberties[merge_group] = np.minimum(sum(liberties[g] for g in merge), 1)

        for g in merge:
            groups[groups==g] = merge_group
            if g != merge_group:
                del liberties[g]
                del colors[g]

        return G

    def check_ladder(self, gnum, c, moves=[]):
        gcolor = self.colors[gnum]

        adjacent = list(group_neighbors(self.position, self.groups, gnum))
        shortage = any(np.sum(self.liberties[g]) < 2 for g in adjacent if g>0 and self.colors[g]!=c)
        lib_indices = np.argwhere(self.liberties[gnum]==1)
        if len(lib_indices) > 2 or shortage:
            return True, moves
        if len(lib_indices) <= 1 and c!=gcolor:
            return False, moves

        all_escape = []
        all_moves = []
        for x in range(lib_indices.shape[0]):
            i,j = lib_indices[x,:]
            nxt = self.play_move(i, j, c)

            ng = nxt.groups[tuple(np.argwhere(self.groups==gnum)[0,:])]

            escape, sub_moves = nxt.check_ladder(ng, 3-c, moves+[(i,j)])
            if c==gcolor and escape:
                return True, sub_moves

            all_escape.append(escape)
            all_moves.append(sub_moves)

        all_lengths = [len(mlist) for mlist in all_moves]
        max_idx = all_lengths.index(max(all_lengths))

        if c!=gcolor and all(all_escape):
            return True, all_moves[max_idx]
        elif c!=gcolor:
            min_idx = sorted((len(mlist), i) for i, (e, mlist) in enumerate(zip(all_escape, all_moves)) if not e)[0][1]
            return False, all_moves[min_idx]
        else:
            return False, all_moves[max_idx]

    def find_ladder_groups(self, min_length=4):
        size = self.position.shape[0]
        position = self.position
        groups = self.groups
        liberties = self.liberties
        colors = self.colors
        check_ladder = self.check_ladder

        escape = [gnum for gnum in liberties if np.sum(liberties[gnum]) == 1]
        capture = [gnum for gnum in liberties if np.sum(liberties[gnum]) == 2]

        for gnum in escape:
            c = colors[gnum]
            status, moves = check_ladder(gnum, c)
            length = len(moves)
            if length>min_length:
                yield gnum, c, status, moves, length

        for gnum in capture:
            c = 3 - colors[gnum]
            status, moves = check_ladder(gnum, c)
            length = len(moves)
            if length>min_length:
                yield gnum, c, status, moves, length

    def enumerate_ladder(self, gnum, color):
        lib_indices = np.argwhere(self.liberties[gnum]==1)

        for x in range(lib_indices.shape[0]):
            i,j = lib_indices[x,:]
            nxt = self.play_move(i, j, color)

            ng = nxt.groups[tuple(np.argwhere(self.groups==gnum)[0,:])]

            escape, sub_moves = nxt.check_ladder(ng, 3-color, [(i,j)])
            length = len(sub_moves)+1
            mv = (i,j)

            yield mv, escape, length

    def find_ladder_moves(self, min_length=4):
        size = self.position.shape[0]
        position = self.position
        groups = self.groups
        liberties = self.liberties
        colors = self.colors
        enumerate_ladder = self.enumerate_ladder

        capture = [gnum for gnum in liberties if np.sum(liberties[gnum]) == 2]

        for gnum in capture:
            c = 3 - colors[gnum]
            for mv, status, length in enumerate_ladder(gnum, c):
                if length>min_length:
                    yield c, mv, status, length

    def to_sgf(self, mark_group=None, add_sequence=None, add_comment=None):
        pos = self.position
        groups = self.groups
        SZ = pos.shape[0]
        blacks = ']['.join(np.apply_along_axis(convert_to_pos, axis=1, arr=np.argwhere(pos==1)))
        whites = ']['.join(np.apply_along_axis(convert_to_pos, axis=1, arr=np.argwhere(pos==2)))
        marked = ''
        if mark_group is not None:
            marked = 'TR[%s]' % (']['.join(np.apply_along_axis(convert_to_pos, axis=1, arr=np.argwhere(groups==mark_group))))
        sequence = ''
        if add_sequence is not None:
            sequence = ';'.join(['%s[%s]' % ('B' if c == 1 else 'W', convert_to_pos(p)) for c,p in add_sequence])
            sequence += ';'
        comment = ''
        if add_comment is not None:
            comment = 'C[%s]' % (add_comment)

        return "(;PB[Black]PW[White]SZ[%d]FF[4]GM[1]CA[UTF-8]%sAB[%s]AW[%s]%s;%s)" % (SZ, comment, blacks, whites, marked, sequence)

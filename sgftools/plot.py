import numpy as np

def plot_go_board(ax, position, markers=None, marker_colors=None, marker_type='^', title=None, add_sequence=None, player=None, stone_size=200, marker_size=100, background='white'):
    x,y = zip(*np.argwhere(position==1))
    ax.scatter(x,y,s=stone_size,color='k', edgecolor='k', marker='o', zorder=10)

    x,y = zip(*np.argwhere(position==2))
    ax.scatter(x,y,s=stone_size,color='white', edgecolor='k', marker='o', zorder=10)

    if marker_colors is None: marker_colors=np.array([[0,1,0]])
    if markers is not None:
        x,y = zip(*np.argwhere(markers==1))
        ax.scatter(x, y, s=marker_size, c=marker_colors, marker=marker_type, edgecolor='none', zorder=11)

    if add_sequence is not None:
        assert player is not None
        c = player
        x,y = zip(*add_sequence)
        ax.scatter(x[::2], y[::2], s=stone_size, marker='o', color='k' if c==1 else 'white', edgecolor='k', zorder=10)
        ax.scatter(x[1::2], y[1::2], s=stone_size, marker='o', color='white' if c==1 else 'k', edgecolor='k', zorder=10)

        for i,(x,y) in enumerate(add_sequence):
            if i%2==0: color='white' if c==1 else 'black'
            else: color='black' if c==1 else 'white'
            ax.text(x, y, str(i+1), color=color, weight='bold', horizontalalignment='center', verticalalignment='center', zorder=11)

    ax.scatter([3,3,3,9,9,9,15,15,15], [3,9,15,3,9,15,3,9,15], s=10, marker='o', color='k', zorder=9)
    ax.vlines(np.arange(19), 0, 18, linewidth=1)
    ax.hlines(np.arange(19), 0, 18, linewidth=1)
    ax.vlines([-0.75, 18.75], -0.75, 18.75, linewidth=2)
    ax.hlines([-0.75, 18.75], -0.75, 18.75, linewidth=2)
    ax.axhspan(-1, 19, facecolor=background, edgecolor='none', zorder=-100)
    for k in ax.spines:
        ax.spines[k].set_visible(False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_xlim(-1,19)
    ax.set_ylim(-1,19)
    if title is not None:
        ax.set_title(title)

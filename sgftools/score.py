from sgftools.ladders import UnionFind
import numpy as np

def score(position):
    reach = np.zeros((position.shape[0], position.shape[1], 2), dtype=np.int32)
    reach[position==1] = [1,0]
    reach[position==2] = [0,1]

    # process reach
    last_reach = reach.copy()
    while 1:
        reach[1:,:]  = (reach[1:,:]==1)  | ((reach[:-1,:]==1) & (np.expand_dims(position[1:,:],axis=-1)==0))
        reach[:-1,:] = (reach[:-1,:]==1) | ((reach[1:,:]==1)  & (np.expand_dims(position[:-1,:],axis=-1)==0))

        reach[:,1:]  = (reach[:,1:]==1)  | ((reach[:,:-1]==1) & (np.expand_dims(position[:,1:],axis=-1)==0))
        reach[:,:-1] = (reach[:,:-1]==1) | ((reach[:,1:]==1)  & (np.expand_dims(position[:,:-1],axis=-1)==0))

        if np.all(last_reach==reach):
            break
        last_reach = reach.copy()


    black = reach[:,:,0]==1
    white = reach[:,:,1]==1
    double = black & white

    occupancy = np.zeros(position.shape, dtype=np.int32)
    occupancy[black&~double] = 1
    occupancy[white&~double] = 2
    return (occupancy==1).sum(), (occupancy==2).sum(), occupancy

==Leela Analysis Engine==

=Installation=

First, install the leela command line engine only version, preferably on your system path somewhere

Next, you'll need the python setuptools package (many configurations will already have this):

    sudo pip install setuptools

If you don't have pip do this first:

    sudo easy_install pip

Next, after cloning the repository:

    hg clone https://bitbucket.org/mkmatlock/sgftools
    cd sgftools

Then install the tools:

    sudo python setup.py build install

_Notes on requirements_

The installation may complain about a missing dependency to numpy. This package is not necessary for using the leela analysis tool and you do not need
to install it. Numpy is used by the alignment tool, which automatically aligns patterns in sgf files for studying opening positions.

In addition, use of the -g option to graph with win rate requires installing matplotlib. This is not required

=Usage=

The typical workflow for sgfanalyze is as follows:

1. Look through your SGF for situations that you need help understanding
2. Add "suggest" to the comments to instruct leela to suggest moves
3. Add "variations" to the comments to instruct leela to add possible follow ups. This option is configurable with the -b (number of branches) and -d (depth of branches) options on the command line
4. Run ```sgfanalyze your_game.sgf [options] -x /path/to/your/leela/executable > output.sgf```

Please note that, by default, the script will store partial results in the folder ~/.leela_checkpoints/, which enables the script to continue where it
left off in the event of a cancellation or failure. You may want to periodically clean this folder.

=Options=

In addition to using "suggest" and "variations" to instruct the analysis engine, additional options are available from the command line. Help is
avaialble via ```sgfanalyze -h```

* -m M and -n N for M, N integer arguments: Provide suggestions all moves between move M and N
* -b B and -d D for B, D integer arguments: For variations commands, explore B branches to a depth of D
* -s R for R integer argument: Supplement each analysis step with R additional runs. This can be used to provide a more thorough positional
  evaluation and is especially useful on slower machines. This will increase the number of visited positions per analysis step.
* -p P for P one of {black, white}: Specify which player you are interested in during the analysis, this currently only modifies the -g option
* -g filename.pdf: Specify an output file to construct a graph of the winrate. This feature requires matplotlib.

==Other Tools Included==

=sgf2pdf=

Extracts positions marked with the text "problem" from sgf files and then convert them to PDFs. Handles multiple sgf files to produce a book of
problems.

=sgfmerge=

Merges multiple sgfs into one. Also contains tools to rotate and align sgfs, which can be used to study a particular opening pattern in several
different professional games. Depends on numpy.

#!/bin/bash
bucket=$1
filehash=$2
shift 2

function save_progress {
    pushd $filehash
    [ -f "part.tar.gz" ] && rm "part.tar.gz"
    if ls log 1> /dev/null 2>&1
    then
        tar -zcf part.tar.gz $( ls log *.pyp 2> /dev/null )
        aws s3 cp part.tar.gz s3://$bucket/$filehash.part.tar.gz
        rm part.tar.gz
    fi
    popd
}

function periodic_save {
    while true
    do
        save_progress
        sleep 60
    done
}

function post_output {
    pushd $filehash
    tar -zcf game.tar.gz out.sgf summary.pdf tsumego/*
    aws s3 cp game.tar.gz s3://$bucket/$filehash.tar.gz
    popd
}

function initialize {
    pushd $filehash
    aws s3 cp s3://$bucket/$filehash.sgf game.sgf
    if aws s3 ls s3://$bucket/$filehash.part.tar.gz ; then
        aws s3 cp s3://$bucket/$filehash.part.tar.gz part.tar.gz
        tar -zxf part.tar.gz
        rm part.tar.gz
    fi
    popd
}

mkdir -p $filehash

initialize

periodic_save $filehash > /dev/null 2>&1 &
backup_pid=$!

function save_progress_and_shutdown_exit {
    kill $backup_pid
    save_progress
    exit 1
}

function save_progress_and_shutdown {
    kill $backup_pid
    save_progress
}

trap save_progress_and_shutdown EXIT
trap save_progress_and_shutdown_exit SIGHUP SIGINT SIGQUIT SIGTERM

python -m leela_ecs.analyze $@ $filehash/game.sgf $filehash > $filehash/log 2>&1
rc=$?

save_progress
if [ $rc -eq 0 ]
then
    post_output
fi

import unittest
from sgftools import gotools, sgflib
import difflib

class TestGoTools(unittest.TestCase):

    def test_get_crop_prob_ts( self ):
        sgf = gotools.import_sgf( 'test/data/4-4-low-approach-two-space-low-pincer-two-space-high-kakari.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("jasj", crop)

    def test_get_crop_prob0001( self ):
        sgf = gotools.import_sgf( 'test/data/prob0001.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("ddpp", crop)

    def test_get_crop_prob0004( self ):
        sgf = gotools.import_sgf( 'test/data/prob0004.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("ajjs", crop)

    def test_get_crop_prob0008( self ):
        sgf = gotools.import_sgf( 'test/data/prob0008.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("jasj", crop)

    def test_get_crop_borderline( self ):
        sgf = gotools.import_sgf( 'test/data/borderline_crop.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("ddpp", crop)

    def test_split_and_add_numberings_xuan_problems( self ):
        for i in range(1, 10):
            fn = 'test/data/prob000%d.sgf' % (i)
            sgf = gotools.import_sgf( fn )
            gotools.split_continuations( sgf )
            gotools.add_numberings( sgf )

    def test_get_crop_lower_left(self):
        sgf = gotools.import_sgf( 'test/data/prob0007.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("ajjs", crop)

    def test_get_crop_upper_middle(self):
        sgf = gotools.import_sgf( 'test/data/two-stone-edge-squeeze-cont.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("dapj", crop)

    def test_get_crop_lower_half(self):
        sgf = gotools.import_sgf( 'test/data/ch1-dia42.sgf' )
        crop = gotools.get_crop( sgf )

        self.assertEqual("ajss", crop)

    def test_add_numberings( self ):
        sgf = gotools.import_sgf( 'test/data/two-stone-edge-squeeze-cont.sgf' )
        gotools.add_numberings( sgf )

        expsgf = ""
        with open( 'test/data/add-numbering-output.sgf', 'r' ) as expfile:
            for line in expfile:
                expsgf += line
        self.assertEqual(expsgf, str(sgf))

    def diffEq(self, str1, str2):
        if str1 == str2: return
        result = "'''" + str1 + "'''" + "\n-----\n" + "'''" + str2 + "'''"
        raise AssertionError("Strings not equal:\n-----\n" + result)

    def test_add_numberings_pass( self ):
        sgf = gotools.import_sgf( 'test/data/pass-example.sgf' )
        gotools.clean_sgf( sgf )
        gotools.add_numberings( sgf )

        expsgf=""
        with open( 'test/data/add-numbering-pass-output.sgf', 'r' ) as expfile:
            for line in expfile:
                expsgf += line

        expsgf = expsgf.strip()
        self.diffEq(expsgf, str(sgf))

    def test_add_numberings_with_edit( self ):
        sgf = gotools.import_sgf( 'test/data/edit-example.sgf' )
        gotools.clean_sgf( sgf )
        gotools.add_numberings( sgf )

        expsgf=""
        with open( 'test/data/add-numbering-edit-output.sgf', 'r' ) as expfile:
            for line in expfile:
                expsgf += line

        expsgf = expsgf.strip()
        self.diffEq(expsgf, str(sgf))


    def test_add_numberings_2( self ):
        sgf = gotools.import_sgf( 'test/data/many-variants.sgf' )
        gotools.add_numberings( sgf )

        expsgf = ""
        with open( 'test/data/add-numbering-output-2.sgf', 'r' ) as expfile:
            for line in expfile:
                expsgf += line
        self.assertEqual(expsgf, str(sgf))

    def test_split_continuations( self ):
        sgf = gotools.import_sgf( 'test/data/two-stone-edge-squeeze-cont.sgf' )
        gotools.split_continuations( sgf )

        expsgf = ""
        with open( 'test/data/cont-capture-output.sgf', 'r' ) as expfile:
            for line in expfile:
                expsgf += line
        self.assertEqual(expsgf, str(sgf))

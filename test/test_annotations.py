import unittest
from sgftools import gotools, sgflib, annotations

class TestAnnotations(unittest.TestCase):
    def test_insert_sequence(self):
        sgf = gotools.import_sgf( 'analyze.sgf' )
        C = sgf.cursor()
        for i in range(15):
            C.next(0)

        def callback(move_num, cursor, elem):
            cnode = cursor.node
            cnode.addProperty( cnode.makeProperty( 'C', [str(move_num)+str(elem)] ) )

        seq = [('white', 'qf'),
               ('black', 'nc'),
               ('white', 'rd'),
               ('black', 'qc'),
               ('white', 'qi')]
        annotations.insert_sequence(15, C, seq, [5,4,3,2,1], callback)

        open('test.sgf', 'w').write(str(sgf))

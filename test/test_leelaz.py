from __future__ import print_function
import unittest
from sgftools import gotools
from leela_ecs import leelaz

def convert_pos(p):
    abet = 'abcdefghijklmnopqrstuvwxyz'
    mapped = 'abcdefghjklmnopqrstuvwxyz'
    X = abet[mapped.index(p[0].lower())]
    Y = abet[19-int(p[1:])]
    return "%s%s" % (X, Y)

def convert_seq(s):
    return [convert_pos(pos) for pos in s.split()]


class LeelazCLITests(unittest.TestCase):
    def test_parse_analysis_0_16(self):
        lz = leelaz.CLI()

        stdout = """ \n= Q16"""
        stderr = open('test/data/leelaz_example_output_0_16.txt', 'r').read()
        summary, move_list = lz.parse(stdout, stderr)

        print(summary)
        print(move_list)

    def test_parse_analysis(self):
        lz = leelaz.CLI()

        stdout = """ \n= Q16"""
        stderr = open('test/data/leelaz_example_output.txt', 'r').read()
        summary, move_list = lz.parse(stdout, stderr)

        print(summary)
        print(move_list)

    def test_parse_analysis_with_pass(self):
        lz = leelaz.CLI()

        stdout = """ \n= N10"""
        stderr = open('test/data/leelaz_example_output_with_pass.txt', 'r').read()
        summary, move_list = lz.parse(stdout, stderr)

        print(summary)
        print(move_list)

    def test_parse_analysis_with_resign(self):
        lz = leelaz.CLI()

        stdout = """ \n= resign"""
        stderr = open('test/data/leelaz_example_output_with_resign.txt', 'r').read()
        summary, move_list = lz.parse(stdout, stderr)

        print(summary)
        print(move_list)

    @unittest.skipIf(True, "Not available")
    def test_analyze(self):
        lz = leelaz.CLI()

        lz.start()

        sgf = gotools.import_sgf('test/data/full_game.sgf')

        C = sgf.cursor()
        for i in range(36):
            C.next()
            if 'W' in C.node.keys():
                lz.add_move('white', C.node['W'].data[0])
            if 'B' in C.node.keys():
                lz.add_move('black', C.node['B'].data[0])

        lz.start()
        lz.reset()
        lz.goto_position()
        stats, move_list = lz.analyze()
        lz.stop()

        print(stats)
        print(move_list)

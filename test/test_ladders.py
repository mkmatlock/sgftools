from sgftools import sgflib
import numpy as np
from sgftools.gotools import Goban, import_sgf
from sgftools import ladders
import unittest

def str_to_npa(s):
    return np.array([[int(v) for v in line.split()] for line in s.split('\n')], dtype=np.int)

class BadukTestCases(unittest.TestCase):
    def setUp(self):
        self.sgf = import_sgf('test/data/test_9x9.sgf')
        self.goban = Goban(self.sgf)
        self.position = self.goban.pattern()

    def test_find_groups(self):
        groups = ladders.Goban(self.position).groups
        exp = """0 10  0  0  0  0  0  0  0
                 0  0  0 12 12 39  0  0  0
                 0 12 12 12 39 39  0  0  0
                 4 12 22 22  0  0  0 67  0
                 0 14  0  0 41 43 43 68  0
                 0  0  0  0  0 43 60  0  0
                 0  0  0 34 43 43 60  0  0
                 0  0  0 34 43 60 60  0  0
                 0  0  0  0 43  0  0  0  0"""

        np.testing.assert_array_almost_equal(groups.T, str_to_npa(exp))

    def test_play_move(self):
        goban = ladders.Goban(self.position).play_move(7,5,2)
        exp = """0 10  0  0  0  0  0  0  0
                 0  0  0 12 12 39  0  0  0
                 0 12 12 12 39 39  0  0  0
                 4 12 22 22  0  0  0 67  0
                 0 14  0  0 41 43 43 68  0
                 0  0  0  0  0 43 60 69  0
                 0  0  0 34 43 43 60  0  0
                 0  0  0 34 43 60 60  0  0
                 0  0  0  0 43  0  0  0  0"""
        np.testing.assert_array_almost_equal(goban.groups.T, str_to_npa(exp))

        exp = """0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 1 0
                 0 0 0 0 0 0 1 0 1
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0"""
        np.testing.assert_array_almost_equal(goban.liberties[67].T, str_to_npa(exp))

        exp = """0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 1
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0"""
        np.testing.assert_array_almost_equal(goban.liberties[68].T, str_to_npa(exp))

        exp = """0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 1
                 0 0 0 0 0 0 0 1 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0"""
        np.testing.assert_array_almost_equal(goban.liberties[69].T, str_to_npa(exp))

        exp = """0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 1 0
                 0 0 0 0 0 0 0 1 0
                 0 0 0 0 0 1 1 0 0"""
        np.testing.assert_array_almost_equal(goban.liberties[60].T, str_to_npa(exp))

    def test_play_move_merge_groups(self):
        goban = ladders.Goban(self.position).play_move(4,3,1)
        exp = """0 10  0  0  0  0  0  0  0
                 0  0  0 12 12 22  0  0  0
                 0 12 12 12 22 22  0  0  0
                 4 12 22 22 22  0  0 67  0
                 0 14  0  0 22 43 43 68  0
                 0  0  0  0  0 43 60  0  0
                 0  0  0 34 43 43 60  0  0
                 0  0  0 34 43 60 60  0  0
                 0  0  0  0 43  0  0  0  0"""
        np.testing.assert_array_equal(goban.groups.T, str_to_npa(exp))

        self.assertTrue(39 not in goban.liberties)
        self.assertTrue(41 not in goban.liberties)

        exp = """0 0 0 0 0 1 0 0 0
                 0 0 0 0 0 0 1 0 0
                 0 0 0 0 0 0 1 0 0
                 0 0 0 0 0 1 0 0 0
                 0 0 1 1 0 0 0 0 0
                 0 0 0 0 1 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0
                 0 0 0 0 0 0 0 0 0"""
        np.testing.assert_array_equal(goban.liberties[22].T, str_to_npa(exp))

    def test_find_ladder_groups(self):
        g = ladders.Goban(self.position)
        result = list(g.find_ladder_groups(min_length=3))
        (gnum, color, status, _moves, length), = result
        self.assertEqual(gnum, 68)
        self.assertEqual(color, 2)
        self.assertEqual(status, False)
        self.assertEqual(length, 4)

    def test_convert_to_sgf(self):
        sgf = ladders.Goban(self.position).to_sgf(mark_group=68, add_sequence=[(2, (7,5)), (1, (8, 4)), (2, (8,5)), (1, (8,3)), (2, (8, 2))])
        self.assertEqual(sgf, '(;PB[Black]PW[White]SZ[9]FF[4]GM[1]CA[UTF-8]\
AB[ad][be][cd][dd][dg][dh][ec][ee][fb][fc][fh][gf][gg][gh][he]AW[ba][bc][bd][cc][db][dc][eb][eg][eh][ei][fe][ff][fg][ge][hd]TR[he];\
W[hf];B[ie];W[if];B[id];W[ic];)')

    def perform_ladder_result_test(self, fn, exp, min_len=4):
        sgf = import_sgf(fn)
        goban = Goban(sgf)
        position = goban.pattern()

        g = ladders.Goban(position)
        result = [(color, status, length) for gnum, color, status, moves, length in g.find_ladder_groups(min_length=min_len)]

        self.assertEqual(result, exp)

    def test_ladder_ex_1(self):
        self.perform_ladder_result_test('test/data/ex_ladder_1.sgf', [(2, False, 11)])

    def test_ladder_ex_2(self):
        self.perform_ladder_result_test('test/data/ex_ladder_2.sgf', [(2, True, 9)])

    def test_ladder_ex_3(self):
        self.perform_ladder_result_test('test/data/ex_ladder_3.sgf', [(1, False, 12)])

    def test_ladder_ex_4(self):
        self.perform_ladder_result_test('test/data/ex_ladder_4.sgf', [(1, True, 8)])

    def test_ladder_ex_5(self):
        self.perform_ladder_result_test('test/data/ex_ladder_5.sgf', [])

    def test_ladder_ex_6(self):
        self.perform_ladder_result_test('test/data/ex_ladder_6.sgf', [(2, False, 10)])

    def test_ladder_ex_7(self):
        self.perform_ladder_result_test('test/data/ex_ladder_7.sgf', [(2, True, 12)])

    def test_ladder_ex_8(self):
        self.perform_ladder_result_test('test/data/ex_ladder_8.sgf', [(2, False, 8)])

    def test_ladder_ex_9(self):
        self.perform_ladder_result_test('test/data/ex_ladder_9.sgf', [(2, False, 8)])

    def perform_ladder_move_test(self, fn, exp, min_len=5):
        sgf = import_sgf(fn)
        goban = Goban(sgf)
        position = goban.pattern()

        g = ladders.Goban(position)
        result = [val for val in g.find_ladder_moves(min_len)]

        self.assertEqual(result, exp)

    def test_ladder_ex_5_moves(self):
        self.perform_ladder_move_test('test/data/ex_ladder_5.sgf', [])

    def test_ladder_ex_6_moves(self):
        self.perform_ladder_move_test('test/data/ex_ladder_6.sgf', [(2, (6,5), False, 11)])

    def test_ladder_ex_8_moves(self):
        self.perform_ladder_move_test('test/data/ex_ladder_8.sgf', [(2, (5,6), False, 9), (2, (6,5), True, 13)])

    def test_ladder_ex_9_moves(self):
        self.perform_ladder_move_test('test/data/ex_ladder_9.sgf', [(2, (6,5), False, 9)])

from sgftools.gotools import Goban, import_sgf
from sgftools.score import score
import numpy as np
import unittest

def to_pattern(s):
    pat = np.array([[int(c) for c in l.strip()] for l in s.split("\n")])
    return pat.T


class TestScoring(unittest.TestCase):
    def test_score_unfinished(self):
        sgf = import_sgf('test/data/test_9x9.sgf')
        goban = Goban(sgf).perform_all()
        position = goban.pattern()
        black,white,occ = score(position)

        coloring = to_pattern("""020000000
                                 000221000
                                 022211000
                                 121100020
                                 010012210
                                 000002100
                                 000122100
                                 000121100
                                 000020000""")
        self.assertEqual(black, 15)
        self.assertEqual(white, 15)
        np.testing.assert_array_almost_equal(occ, coloring)

    def test_score_with_uncaptured(self):
        sgf = import_sgf('test/data/test_9x9_finished_with_stones.sgf')
        goban = Goban(sgf).perform_all()
        position = goban.pattern()
        black,white,occ = score(position)

        coloring = to_pattern("""220021112
                                 221221122
                                 222211222
                                 121112222
                                 111112212
                                 111112111
                                 111122100
                                 222121122
                                 222221110""")
        self.assertEqual(black, 37)
        self.assertEqual(white, 39)
        np.testing.assert_array_almost_equal(occ, coloring)


    def test_score(self):
        sgf = import_sgf('test/data/test_9x9_finished.sgf')
        goban = Goban(sgf).perform_all()
        position = goban.pattern()
        black,white,occ = score(position)

        coloring = to_pattern("""222221112
                                 222221122
                                 222211222
                                 121112222
                                 111112212
                                 111112111
                                 111122111
                                 222121111
                                 222221111""")
        self.assertEqual(black, 41)
        self.assertEqual(white, 40)
        np.testing.assert_array_almost_equal(occ, coloring)

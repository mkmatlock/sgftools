
 N10 ->     964 (V:  2.49%) (N:  7.16%) PV: N10 M10 M9 N11 O9 N8 O8 O7 N7 M8 L9 M7 N6 M6 N5 O6
 G15 ->     585 (V:  2.79%) (N:  1.62%) PV: G15 G14 H15 J14 F14 E15 H14 H13 G13 J15 G14 J13 E19 E11 G11 D12
  O9 ->     495 (V:  2.36%) (N:  4.68%) PV: O9 O8 N8 O7 M9 N10 O11 M8 N7 N6 M7 L8 L7 K7 K8 L9 J7
 N11 ->     333 (V:  2.39%) (N:  3.22%) PV: N11 N7 B4 A4 B2 B10 B9 C9 B8
 O11 ->     266 (V:  2.54%) (N:  1.97%) PV: O11 N7 P10 Q11 O12 R8 S8 S13 S12 T12 S11 B3 C2
 P10 ->     147 (V:  2.41%) (N:  1.43%) PV: P10 Q11 O8 O11 N10 N11 L9 M10 M9 N8
 A10 ->     133 (V:  2.87%) (N:  0.34%) PV: A10 B9 B10 B3 C2 B4 B2 B7 A2 O2 P2
  B4 ->     123 (V:  2.17%) (N:  1.69%) PV: B4 A4 B2 B10 B9 C9 B8 D10 C11 B7 A10 E9 E11 G9
 S12 ->      81 (V:  2.47%) (N:  0.74%) PV: S12 S15 N10 M10 M9 N8 N11 L9
 B10 ->      76 (V:  2.69%) (N:  0.43%) PV: B10 B3 C2 B4 B2 B7 A2 O2 P2 O3 O4
  B9 ->      70 (V:  2.17%) (N:  0.97%) PV: B9 B3 C2 B4 C7 B7 B8 D7 D8 C6
  N7 ->      67 (V:  2.40%) (N:  0.68%) PV: N7 O9 N11 N10 O11 K10 B4 A4
  O7 ->      66 (V:  2.36%) (N:  0.71%) PV: O7 O8 N7 L9 O11 O12 N12 R8
  H6 ->      64 (V:  2.31%) (N:  0.74%) PV: H6 B3 C2 B4 K7 N11 O7 O8 N7
 N18 ->      59 (V:  2.47%) (N:  0.54%) PV: N18 M19 G15 G14 H15 J14 F14 E15 H14 H13
  B2 ->      59 (V:  2.32%) (N:  0.68%) PV: B2 B4 B3 B9 B10 B7 N11 O9 M10 N10 O11
 B17 ->      57 (V:  2.66%) (N:  0.35%) PV: B17 B3 C2 B4 H6 E19 N10 M10 M9
  N2 ->      57 (V:  2.41%) (N:  0.58%) PV: N2 M2 N10 M10 N11 M8 M12 B3
  J7 ->      57 (V:  2.29%) (N:  0.69%) PV: J7 B3 C2 B4 N10 M10 M9 N11 O9
  L9 ->      56 (V:  2.40%) (N:  0.58%) PV: L9 N7 N10 M10 M9 N11 N8 O9 O8
 T12 ->      54 (V:  2.64%) (N:  0.35%) PV: T12 S13 S12 S15 N11 N7 B4 A4 B2 B10 B9
  E8 ->      53 (V:  2.12%) (N:  0.77%) PV: E8 B3 C2 B4 B7 B8 C7 D7
 M19 ->      49 (V:  2.40%) (N:  0.49%) PV: M19 N19 N18 K19 O19 L19 Q19 R19 S19 P19 M19 Q19 S15 R15
  N4 ->      49 (V:  2.39%) (N:  0.52%) PV: N4 N3 O3 N5 M4 M5 O4 L3
  E9 ->      48 (V:  2.38%) (N:  0.51%) PV: E9 B3 C2 B4 C7 B7 D7 B9 D8
  M9 ->      45 (V:  2.38%) (N:  0.47%) PV: M9 N10 O11 M8 P10 Q11 O8 O7
  F8 ->      45 (V:  2.26%) (N:  0.56%) PV: F8 B3 C2 B4 N10 M10 M9 N11
  R8 ->      44 (V:  2.50%) (N:  0.38%) PV: R8 O9 B4 A4 B2 B10 B9 C9 B8 D10
  N6 ->      44 (V:  2.49%) (N:  0.38%) PV: N6 N11 K10 L9 K9 L8 J7 B3
  P5 ->      43 (V:  2.54%) (N:  0.35%) PV: P5 N11 B4 A4 B2 B10 B9 C9 B8 D10 C11
 F12 ->      43 (V:  2.52%) (N:  0.36%) PV: F12 B3 B2 B4 C2 C9 B10 B9 H6 O2
 Q19 ->      43 (V:  2.40%) (N:  0.45%) PV: Q19 R19 P19 S19 N10 M10 M9 N8
  K7 ->      43 (V:  2.39%) (N:  0.45%) PV: K7 B3 C2 B4 N10 M10 O9 O8
 F14 ->      43 (V:  2.19%) (N:  0.59%) PV: F14 G15 N11 O9 B4 A4 B2 B10
  O5 ->      42 (V:  2.50%) (N:  0.37%) PV: O5 N11 B4 A4 B2 B10 B9 C9
 L11 ->      42 (V:  2.39%) (N:  0.44%) PV: L11 B3 C2 B4 N7 L10 O9 O4
 D10 ->      41 (V:  2.54%) (N:  0.33%) PV: D10 B3 C2 B4 N11 O9 L9 M10
 M15 ->      40 (V:  2.54%) (N:  0.33%) PV: M15 B3 C2 B4 H6 K7 J7 K8 K6
 E11 ->      39 (V:  2.46%) (N:  0.37%) PV: E11 B3 C2 B4 J7 N11 K10 L9 K9
  O4 ->      38 (V:  2.49%) (N:  0.34%) PV: O4 N11 B4 A4 B2 B10 B9 C9 B8 D10
 H12 ->      38 (V:  2.46%) (N:  0.36%) PV: H12 B3 C2 B4 J7 N11 G15 G14
  O2 ->      38 (V:  2.41%) (N:  0.39%) PV: O2 N2 B4 A4 B2 B10 B9 C9 B8
  C9 ->      38 (V:  2.35%) (N:  0.42%) PV: C9 D8 B4 A4 B2 B9 B10 A9
  J8 ->      38 (V:  2.33%) (N:  0.43%) PV: J8 N11 B9 B8 B4 C9 B10
 H11 ->      37 (V:  2.47%) (N:  0.34%) PV: H11 B3 C2 B4 N10 M10 M9 N8
 O19 ->      37 (V:  2.45%) (N:  0.36%) PV: O19 N18 N11 N7 B4 A4 B2 B10 B9 C9 B8
  L7 ->      37 (V:  2.41%) (N:  0.38%) PV: L7 N11 K10 N7 B4 A4 B2 B9 B10
  O6 ->      37 (V:  2.41%) (N:  0.38%) PV: O6 N7 N11 B3 C2 B4 M5
  M7 ->      37 (V:  2.38%) (N:  0.39%) PV: M7 N11 O7 O8 P7 P8 L9 L11
  R9 ->      37 (V:  2.37%) (N:  0.39%) PV: R9 Q9 N11 N7 B4 A4 B2 B10 B9 C9
  F9 ->      37 (V:  2.22%) (N:  0.49%) PV: F9 B3 C2 B4 N10 M10 M9 N8 N11
  M6 ->      36 (V:  2.45%) (N:  0.35%) PV: M6 N11 B4 A4 B2 B10 B9 C9 B8
  A8 ->      36 (V:  2.43%) (N:  0.36%) PV: A8 B9 B10 A9 B4 A4 B2 pass C7 D7
 F11 ->      36 (V:  2.41%) (N:  0.37%) PV: F11 B3 B2 B4 C2 B9 N10 M10 M9 N8
  K9 ->      36 (V:  2.39%) (N:  0.38%) PV: K9 B3 C2 B4 O9 O8 N8 O7 M8
 P19 ->      36 (V:  2.39%) (N:  0.38%) PV: P19 Q19 N10 M10 O9 O8 N8
  K8 ->      36 (V:  2.31%) (N:  0.41%) PV: K8 B3 C2 B4 N7 O9 N11 L9 L11
 E19 ->      36 (V:  2.31%) (N:  0.42%) PV: E19 E17 N10 M10 O9 O8 N8
  A9 ->      35 (V:  2.45%) (N:  0.34%) PV: A9 B10 B9 C9 D10 B8 B11 B3
 G12 ->      35 (V:  2.43%) (N:  0.34%) PV: G12 B3 C2 B4 G15 G14 H15 H14 J15
  P6 ->      35 (V:  2.41%) (N:  0.35%) PV: P6 O7 B4 A4 B2 B10 B9 C9 B8
  M5 ->      35 (V:  2.40%) (N:  0.36%) PV: M5 L5 L6 M6 M7 N6 N7 O6 O7 P6
  M8 ->      35 (V:  2.38%) (N:  0.38%) PV: M8 M9 L9 N8 L8 N6 L11 B3 C2
  L8 ->      35 (V:  2.37%) (N:  0.38%) PV: L8 N7 O11 B3 C2 B4 M6 N6
 G11 ->      34 (V:  2.42%) (N:  0.34%) PV: G11 B3 C2 B4 H6 N11 G15 G14
 E12 ->      34 (V:  2.41%) (N:  0.34%) PV: E12 B3 C2 B4 G15 F14 G14
  N3 ->      34 (V:  2.41%) (N:  0.35%) PV: N3 N2 N4 O2 O9 O8 N8 O7 M9
  J9 ->      34 (V:  2.40%) (N:  0.36%) PV: J9 B3 C2 B4 N10 M10 M9 N8
 K10 ->      34 (V:  2.34%) (N:  0.38%) PV: K10 L9 K9 L8 K8 B3 C2 B4 K7 M6
  N8 ->      34 (V:  2.29%) (N:  0.42%) PV: N8 O9 M9 N10 O8 L11 K11
  H8 ->      33 (V:  2.33%) (N:  0.38%) PV: H8 B3 C2 B4 N10 M10 M9 N11 O9
 G13 ->      33 (V:  2.30%) (N:  0.40%) PV: G13 G14 F13 F14 B4 B9 B7 B10
  G8 ->      33 (V:  2.26%) (N:  0.42%) PV: G8 B3 C2 B4 O9 O8 N8 O7 M9
  B8 ->      33 (V:  2.13%) (N:  0.50%) PV: B8 B9 C9 B7 D8 C7 B10 A8
  G7 ->      32 (V:  2.42%) (N:  0.33%) PV: G7 B3 C2 B4 N10 M10 M9 N11 O9 N8
 J10 ->      32 (V:  2.37%) (N:  0.35%) PV: J10 N11 B4 A4 B2 B10 B9
 S15 ->      32 (V:  2.36%) (N:  0.35%) PV: S15 R15 S12 S14 N11 N7 B4 A4
  H7 ->      32 (V:  2.36%) (N:  0.35%) PV: H7 B3 C2 B4 O9 O8 N8 O7
 H10 ->      32 (V:  2.34%) (N:  0.35%) PV: H10 B3 C2 B4 N10 M10 O9 O8
 E10 ->      32 (V:  2.33%) (N:  0.37%) PV: E10 B3 C2 B4 N10 M10 M9 N11
 J11 ->      31 (V:  2.36%) (N:  0.34%) PV: J11 B3 C2 B4 O9 O8 N8 O7
 F13 ->      31 (V:  2.35%) (N:  0.34%) PV: F13 F14 B4 A4 B2 B10
  H9 ->      31 (V:  2.31%) (N:  0.37%) PV: H9 B3 C2 B4 N10 M10 M9 N8 N11
  G9 ->      31 (V:  2.22%) (N:  0.41%) PV: G9 B3 C2 B4 N10 M10 O9 O8 N8
 H13 ->      30 (V:  2.35%) (N:  0.33%) PV: H13 G14 N10 M10 M9 N8 N11
  L5 ->      30 (V:  2.31%) (N:  0.36%) PV: L5 M5 M6 N5 N6 O5 O6 P6
  B3 ->      30 (V:  2.30%) (N:  0.36%) PV: B3 B9 B10 B7 N10 M10 O9 O8 N8 M9
 G10 ->      30 (V:  2.29%) (N:  0.37%) PV: G10 B3 C2 B4 O9 O8 N8 O7 M9
 G14 ->      30 (V:  2.27%) (N:  0.37%) PV: G14 G15 N11 O9 L9 M10 K10
 L10 ->      29 (V:  2.33%) (N:  0.34%) PV: L10 L11 K11 K10 L9 K12 J11
  A7 ->      29 (V:  2.29%) (N:  0.35%) PV: A7 B7 B9 B3 C2 B4 B8 C7
 F10 ->      29 (V:  2.21%) (N:  0.39%) PV: F10 B3 C2 B4 N10 M10 M9 N11
  D9 ->      28 (V:  2.28%) (N:  0.35%) PV: D9 B3 B2 B4 C2 D8 F8
  E7 ->      28 (V:  2.22%) (N:  0.37%) PV: E7 B3 C2 B4 N10 M10 M9 N11 O9
  D8 ->      27 (V:  2.16%) (N:  0.39%) PV: D8 C9 D9 B10 C7 B7 D7 B8 B4 A4
  C7 ->      25 (V:  2.13%) (N:  0.38%) PV: C7 B7 D7 C9 B6 C6 B8 A6 B9
  B7 ->      22 (V:  2.07%) (N:  0.35%) PV: B7 B8 C7 D7 D8 D9 E8
7.2 average depth, 22 max depth
5068 non leaf nodes, 1.30 average children
6596 visits, 1592078 nodes, 6130 playouts, 90 n/s

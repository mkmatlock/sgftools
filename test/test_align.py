from sgftools import gotools, sgflib
from sgftools.align import align_by_pattern, Transform, apply_alignment
import unittest
from pkg_resources import resource_filename


class AlignTests(unittest.TestCase):
    def setUp(self):
        pattern_sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/Pattern.sgf'))
        pattern_area = 'jasj'
        p_goban = gotools.Goban( pattern_sgf )
        p_goban.perform( pattern_sgf.cursor().node )
        self.pattern = gotools.Pattern( p_goban.boardstate, pattern_area )

    def get_state(self, sgf, iters=10):
        goban = gotools.Goban( sgf )
        c = sgf.cursor()
        goban.perform( c.node )
        for i in range(iters):
            c.next( 0 )
            goban.perform( c.node )
        return goban

    def test_transform(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/1.sgf') )
        T = Transform( sgf, 19 )
        self.assertEqual( 'as', T.apply_rot90('aa') )
        self.assertEqual( 'ss', T.apply_rot90('as') )
        self.assertEqual( 'sa', T.apply_rot90('ss') )
        self.assertEqual( 'aa', T.apply_rot90('sa') )

        self.assertEqual( 'fq', T.apply_rot90('cf') )
        self.assertEqual( 'qn', T.apply_rot90('fq') )
        self.assertEqual( 'nc', T.apply_rot90('qn') )
        self.assertEqual( 'cf', T.apply_rot90('nc') )

        S1 = self.get_state(sgf, 7)
        T.rot90()
        S2 = self.get_state(sgf, 7)
        T.rot90()
        S3 = self.get_state(sgf, 7)
        T.rot90()
        S4 = self.get_state(sgf, 7)
        T.fliplr()
        S5 = self.get_state(sgf, 7)

        self.assertEqual('b', S1.boardstate[16][14])
        self.assertEqual('b', S1.boardstate[13][16])

        self.assertEqual('b', S2.boardstate[14][2])
        self.assertEqual('b', S2.boardstate[16][5])

        self.assertEqual('b', S3.boardstate[2][4])
        self.assertEqual('b', S3.boardstate[5][2])

        self.assertEqual('b', S4.boardstate[2][13])
        self.assertEqual('b', S4.boardstate[4][16])

        self.assertEqual('b', S5.boardstate[16][13])
        self.assertEqual('b', S5.boardstate[14][16])

    def test_align_1(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/1.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_2(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/2.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 5 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['fliplr', 'rot90', 'rot90', 'rot90'], ops)

    def test_align_3(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/3.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_4(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/4.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_5(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/5.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 6 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_6(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/6.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_7(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/7.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 11 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['fliplr', 'rot90', 'rot90'], ops)

    def test_align_8(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/8.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['fliplr'], ops)

    def test_align_9(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/9.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 5 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['rot90'], ops)

    def test_align_10(self):
        sgf = gotools.import_sgf( resource_filename('test', 'data/alignment/10.sgf') )

        nsgf, (state, ops) = align_by_pattern( self.pattern, sgf )
        nstate = self.get_state( sgflib.SGFParser( str(nsgf) ).parse(), 9 )

        self.pattern.assert_matches_seed_state(nstate)
        self.assertEqual(['fliplr'], ops)

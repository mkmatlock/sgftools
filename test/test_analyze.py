import unittest
from sgftools import leela

def convert_pos(p):
    abet = 'abcdefghijklmnopqrstuvwxyz'
    mapped = 'abcdefghjklmnopqrstuvwxyz'
    X = abet[mapped.index(p[0].lower())]
    Y = abet[19-int(p[1:])]
    return "%s%s" % (X, Y)

def convert_seq(s):
    return [convert_pos(pos) for pos in s.split()]

class LeelaCLITests(unittest.TestCase):
    def test_parse_book_moves(self):
        stdout = "= R6"
        stderr = "4 book moves, 163 total positions"
        C = leela.CLI()

        stats, move_list = C.parse(stdout, stderr)
        self.assertEqual({'chosen': convert_pos('R6'), 'bookmoves': 4,
                           'positions': 163}, stats)
        self.assertEqual({convert_pos('R6'): {'visits': 0, 'W': 0}}, move_list)

    def test_parse_status(self):
        sample_status = open('test/data/sample_status.txt', 'r')
        C = leela.CLI()

        expectations = [{},
                        {'visits':2033, 'winrate':0.5071, 'seq': convert_seq('H12 H11 J11 H10 J10 J9 J12 K9')},
                        {'visits':4845, 'winrate':0.5084, 'seq': convert_seq('H12 H11 J11 H10 J12 J10 K10 K9 L10 L9 M10 M9 N10 N9')},
                        {'visits':7488, 'winrate':0.5046, 'seq': convert_seq('H12 H11 J11 H10 J12 J10 K10 K9 L10 L9 M10 M9 N10 J4 L4')},
                        {'visits':10951, 'winrate':0.5051, 'seq': convert_seq('H12 H11 J11 H10 J12 J10 K10 K9 L10 L9 M10 M9 N10 K4 N9 M4')},
                        {}, {}]


        actual = [C.parse_status_update(line.strip()) for line in sample_status]
        self.assertEqual(expectations, actual)

    def test_parse_analysis_2(self):
        stdout = "= F16"
        sample_analysis = open('test/data/sample_analysis_2.txt', 'r').read()
        C = leela.CLI()
        stats, move_dict = C.parse(stdout, sample_analysis)

        self.assertEqual({'mc_winrate': 0.390625,
                           'visits': 4, 
                           'winrate': -0.007,
                           'nn_winrate': 0.454054, 
                           'margin': 'B+14.7', 
                           'best': convert_pos('D15'),
                           'chosen': convert_pos('F16')},
                           stats)

        self.assertEqual(set(convert_seq('D15 F16 E15 E16 G17 C16')),
                          set(move_dict.keys()))

    def test_parse_analysis_3(self):
        stdout = "= C6"
        sample_analysis = open('test/data/sample_analysis_leela_010.txt', 'r').read()

        C = leela.CLI()
        stats, move_dict = C.parse(stdout, sample_analysis)

        exp_stats = {'best': convert_pos('C6'),
                           'winrate': 0.4653,
                           'visits': 10091,
                           'chosen': convert_pos('C6')}
        self.assertEqual(exp_stats, {k: stats[k] for k in exp_stats})

        exp_moves = {convert_pos('C6'):{'visits': 6460,
                                 'seq': convert_seq('C6 B4 C8 G3 O17 R10 R12 R7 Q4 R3'),
                                 'W': 0.4653},
                     convert_pos('C8'):{'visits': 1863,
                                 'seq': convert_seq('C8 C9 E8 D9 F3 C6 C7 B7'),
                                 'W': 0.4498},
                     convert_pos('E8'):{'visits': 1656,
                                 'seq': convert_seq('E8 E9 F8 C8 F9 E10 F10 E11 F11'),
                                 'W': 0.4636},
                     convert_pos('B6'):{'visits': 112,
                                 'seq': convert_seq('B6 C6 C7 B7 C8 C9'),
                                 'W': 0.4403},
                     convert_pos('C7'):{'visits': 0,
                                 'seq': convert_seq('C7'),
                                 'W': 0.0 },
                     convert_pos('B7'):{'visits': 0,
                                 'seq': convert_seq('B7'),
                                 'W': 0.0 }}
        self.assertEqual(set(exp_moves.keys()), set(move_dict.keys()))
        for mv in exp_moves:
            self.assertEqual(exp_moves[mv], {k:move_dict[mv][k] for k in exp_moves[mv]})

    def test_parse_analysis(self):
        stdout = "= H11"
        sample_analysis = open('test/data/sample_analysis.txt', 'r').read()

        C = leela.CLI()
        stats, move_dict = C.parse(stdout, sample_analysis)

        exp_stats = {'best': convert_pos('H12'),
                           'winrate': 0.5165,
                           'visits': 23462,
                           'chosen': convert_pos('H11')}
        self.maxDiff=None
        self.assertEqual(exp_stats, {k: stats[k] for k in exp_stats})
        exp_moves = {convert_pos('H12'):{'visits': 17298, 
                                 'seq': convert_seq('H12 H13 J12 H14 G15 K14 K16 L16 L17 J16 J17'),
                                 'W': 0.5165},
                           convert_pos('H13'):{'visits': 5170,
                                 'seq': convert_seq('H13 J11 K4 H4 J6 G6 C6 D5 B5 B4 B6'),
                                 'W': 0.4975},
                           convert_pos('J13'):{'visits': 610,
                                 'seq': convert_seq('J13 H13 H14 H12 K14 K11'),
                                 'W': 0.5070},
                           convert_pos('C12'):{'visits': 170,
                                 'seq': convert_seq('C12 C11 B12 A12'),
                                 'W': 0.5219},
                           convert_pos('E18'):{'visits': 90,
                                 'seq': convert_seq('E18 F17 E17'),
                                 'W': 0.4966},
                           convert_pos('B12'):{'visits': 33,
                                 'seq': convert_seq('B12 B11'),
                                 'W': 0.4288}}
        self.assertEqual(set(exp_moves.keys()), set(move_dict.keys()))
        for mv in exp_moves:
            self.assertEqual(exp_moves[mv], {k:move_dict[mv][k] for k in exp_moves[mv]})

import os
import sys
import re
import time
import fcntl
import hashlib
import multiprocessing
from subprocess import Popen, PIPE, STDOUT

def set_non_blocking(fd):
    """
    Set the file description of the given file descriptor to
    non-blocking.
    """
    flags = fcntl.fcntl(fd, fcntl.F_GETFL)
    flags = flags | os.O_NONBLOCK
    fcntl.fcntl(fd, fcntl.F_SETFL, flags)

def check_stream(fd):
    try:
        s = fd.read()
        if s is None: return ""
        return s.decode()
    except IOError:
        pass
    return ""

class CLI(object):
    def __init__(self, eval_limit=None, executable='leelaz', verbosity=0, timeout=200):
        self.history=[]
        self.eval_limit=eval_limit
        self.executable = executable
        self.verbosity = verbosity
        self.timeout=timeout

    def convert_position(self, pos):
        if pos == 'tt': return 'pass'
        abet = 'abcdefghijklmnopqrstuvwxyz'
        mapped = 'abcdefghjklmnopqrstuvwxyz'
        pos = '%s%d' % (mapped[abet.index(pos[0])], 19-abet.index(pos[1]))
        return pos

    def parse_position(self, pos):
        if pos == 'pass': return 'tt'
        abet = 'abcdefghijklmnopqrstuvwxyz'
        mapped = 'abcdefghjklmnopqrstuvwxyz'

        X = mapped.index(pos[0].lower())
        Y = 19-int(pos[1:])

        return "%s%s" % (abet[X], abet[Y])

    def history_hash(self):
        H = hashlib.md5()
        for c,p in self.history:
            H.update(c[0] + p)
        return H.hexdigest()

    def add_move(self, color, pos):
        self.history.append((color,pos))

    def clear_history(self):
        self.history = []

    def whoseturn(self):
        if len(self.history) == 0 or self.history[-1][0]=='white':
            return 'black'
        return 'white'

    def parse_status_update(self, message):
        status_regex = r'Playouts: ([0-9]+), Win: ([0-9]+\.[0-9]+)\%, PV:(( [A-Z][0-9]+)+)'

        M = re.match(status_regex, message)
        if M is not None:
            playouts = int(M.group(1))
            winrate = self.to_fraction(M.group(2))
            seq = M.group(3)
            seq = [self.parse_position(p) for p in seq.split()]

            return {'playouts': playouts, 'winrate': winrate, 'seq': seq}
        return {}

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

    def sendcmd(self, cmd):
        self.p.stdin.write(cmd.encode())
        self.p.stdin.flush()
        if self.verbosity>2:
            print(">>> %s" % (cmd), file=sys.stderr)

    def start(self):
        threads = multiprocessing.cpu_count()
        xargs = ['--resignpct', '0', '--threads', str(threads)]
        if self.eval_limit is not None:
            xargs = ['--playouts', str(self.eval_limit)]

        cmd = [self.executable, '--gtp', '--noponder'] + xargs
        print("Start: %s" % (' '.join(cmd)), file=sys.stderr)
        p = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=PIPE)
        set_non_blocking(p.stdout)
        set_non_blocking(p.stderr)
        self.p = p

        time.sleep(5)
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

    def stop(self):
        p = self.p
        self.sendcmd('exit\n')
        p.terminate()
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

    def play_move(self, color, pos):
        p = self.p
        self.history.append((color, pos))
        if pos == '' or pos =='tt':
            pos = 'pass'
        else:
            pos = self.convert_position(pos)
        cmd = 'play %s %s\n' % (color, pos)
        self.sendcmd(cmd)
        time.sleep(0.1)
        sout = check_stream(p.stdout)

        if '? illegal move' in sout:
            raise ValueError("Illegal move: %s %s" % (color, pos))
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", sout, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

    def undo(self, num=None):
        p = self.p
        if num is None:
            self.sendcmd('undo\n')
            mv = self.history.pop()
        else:
            self.sendcmd('gg-undo %d\n' % (num))
            for i in xrange(num): mv = self.history.pop()
        time.sleep(0.1)
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)
        return mv

    def reset(self):
        p = self.p
        self.sendcmd('clear_board\n')
        time.sleep(0.1)
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

    def boardstate(self):
        p = self.p
        self.sendcmd("showboard\n")
        time.sleep(1)
        _out = check_stream(p.stdout)
        err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", err, file=sys.stderr)
        return err

    def goto_position(self):
        p = self.p
        for color, pos in self.history:
            if pos == '' or pos =='tt':
                pos = 'pass'
            else:
                pos = self.convert_position(pos)
            cmd = 'play %s %s\n' % (color, pos)
            self.sendcmd(cmd)
        time.sleep(1)
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

    def analyze(self):
        p = self.p
        if self.verbosity > 1:
            print("Analyzing state:", file=sys.stderr)
            print(self.whoseturn(), "to play", file=sys.stderr)
            print(self.boardstate(), file=sys.stderr)

        cmd = "genmove %s\n" % (self.whoseturn())
        self.sendcmd(cmd)

        updated = 0
        stderr = []
        stdout = []
        finished_regex = '= [A-Z][0-9]+'

        if self.verbosity > 0:
            print("", file=sys.stderr)
        while updated < self.timeout:
            O = check_stream(p.stdout)
            stdout.append(O)

            L = check_stream(p.stderr)
            stderr.append(L)

            D = self.parse_status_update(L)
            if 'playouts' in D:
                if self.verbosity > 0:
                    print("\r%d playouts" % (D['playouts']), file=sys.stderr)
                updated = 0
            updated += 1
            if self.verbosity > 0:
                print("", file=sys.stderr)

            if re.search(finished_regex, ''.join(stdout)) is not None:
                break
            time.sleep(1)

        if self.verbosity > 1:
            print("", file=sys.stderr)

        self.sendcmd("\n")
        time.sleep(1)

        stderr = ''.join(stderr) + check_stream(p.stderr)
        stdout = ''.join(stdout) + check_stream(p.stdout)

        stats, move_list = self.parse(stdout, stderr)
        if self.verbosity > 2:
            print(stats, file=sys.stderr)
            print(move_list, file=sys.stderr)
        if self.verbosity > 0:
            print("Chosen move: %s" % (stats['chosen']), file=sys.stderr)
            if stats['chosen'] != 'resign':
                print("Winrate: %f" % (move_list[stats['chosen']]['V']), file=sys.stderr)
            print("Playouts: %d" % (stats['playouts']), file=sys.stderr)

        self.sendcmd('undo\n')
        time.sleep(0.1)
        _out = check_stream(p.stdout)
        _err = check_stream(p.stderr)
        if self.verbosity>2:
            print("STDOUT:\n", _out, file=sys.stderr)
            print("STDERR:\n", _err, file=sys.stderr)

        return stats, move_list

    def to_fraction(self, v):
        v = v.strip()
        mul=1
        if v.startswith('-'):
            mul=-1
            v = v[1:]

        W, D = v.split('.')
        if len(W) == 1:
            W = "0" + W
        return mul * float('0.' + ''.join([W,D]))

    def parse(self, stdout, stderr):
        if self.verbosity > 2:
            print("STDOUT", file=sys.stderr)
            print(stdout, file=sys.stderr)
            print("STDERR", file=sys.stderr)
            print(stderr, file=sys.stderr)

        move_regex = r'^([A-Z][0-9]+) -> +( +[0-9]+) \(V: +(\-?[0-9]+\.[0-9]+)\%\) \(N: +([0-9]+\.[0-9]+)\%\) PV: (.*)$'
        move_regex_0_16 = r'^([A-Z][0-9]+) -> +( +[0-9]+) \(V: +(\-?[0-9]+\.[0-9]+)\%\) \(LCB: +(\-?[0-9]+\.[0-9]+)\%\) \(N: +([0-9]+\.[0-9]+)\%\) PV: (.*)$'
        summary_regex = r'([0-9]+) visits, ([0-9]+) nodes, ([0-9]+) playouts, ([0-9]+) n/s'

        summary = {}
        move_list = {}

        finished_regex = r'= ([A-Z][0-9]+)'
        M = re.search(finished_regex, stdout)
        if M is not None:
            summary['chosen'] = self.parse_position(M.group(1))
        resign_regex = r'= resign'
        M = re.search(resign_regex, stdout)
        if M is not None:
            summary['chosen'] = 'resign'

        for line in stderr.split('\n'):
            line = line.strip()

            M = re.match(move_regex, line)
            if M is not None:
                start = self.parse_position(M.group(1))
                visits = int(M.group(2))
                V = self.to_fraction(M.group(3))
                N = self.to_fraction(M.group(4))
                seq = M.group(5)
                seq = [self.parse_position(p) for p in seq.split()]

                move_list[start] = {'visits': visits, 'V': V, 'L': None, 'N': N, 'seq': seq}

            M2 = re.match(move_regex_0_16, line)
            if M2 is not None:
                start = self.parse_position(M2.group(1))
                visits = int(M2.group(2))
                V = self.to_fraction(M2.group(3))
                L = self.to_fraction(M2.group(4))
                N = self.to_fraction(M2.group(5))
                seq = M2.group(6)
                seq = [self.parse_position(p) for p in seq.split()]

                move_list[start] = {'visits': visits, 'V': V, 'L': L, 'N': N, 'seq': seq}

            M = re.match(summary_regex, line)
            if M is not None:
               summary['visits'] = int(M.group(1))
               summary['nodes'] = int(M.group(2))
               summary['playouts'] = int(M.group(3))
               summary['rate'] = int(M.group(4))

        return summary, move_list

class DummyCLI(CLI):
    def undo(self, num=None):
        if num is None:
            mv = self.history.pop()
        else:
            for i in xrange(num): mv = self.history.pop()
        return mv

    def play_move(self, color, pos):
        self.history.append((color, pos))

    def analyze(self):
        pass

    def start(self):
        pass

    def stop(self):
        pass

    def boardstate(self):
        pass

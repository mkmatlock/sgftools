import os
import yaml

def update_config(config, extra):
    for key in extra:
        if key not in config:
            config[key] = extra[key]
        elif type(config[key]) is str:
            config[key] = extra[key]
        elif type(config[key]) is dict:
            for k2 in extra[key]:
                config[key].update( extra[key] )

def load_configuration():
    user_path = os.path.expanduser('~/.leela_ecs')
    config = {}
    if os.path.exists(user_path) and not os.path.isdir(user_path):
        with open(user_path, 'r') as cfg_file:
            update_config( config, yaml.load(cfg_file) )
    return config

from __future__ import print_function
from leela_ecs import leelaz
from sgftools import gotools, sgflib
import numpy as np
import os, sys
import pickle
import click
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import pyplot as plt

def logit(x):
    return np.log(x) - np.log(1-x)

def plogodds(x, baseline=0.5):
    return logit(x+baseline)-logit(baseline)

move_classifications = ['EXCELLENT', 'GOOD', 'NEUTRAL', 'ACCEPTABLE', 'INTERESTING',
                        'INACCURACY', 'MISTAKE', 'BLUNDER']
analysis_cutoff = plogodds(-0.05)
move_cutoffs = [ ('EXCELLENT', plogodds(0.05)),
                 ('GOOD', plogodds(0.02)),
                 ('NEUTRAL', 0),
                 ('ACCEPTABLE', plogodds(-0.02)),
                 ('INACCURACY', plogodds(-0.05)), 
                 ('MISTAKE', plogodds(-0.1)), 
                 ('BLUNDER', -np.inf) ]
no_explore_winrate = 0.1
exploration_cutoff = plogodds(-0.02)
exploration_visits_fraction = 0.75
display_cutoff = plogodds(-0.05)
display_visits = 100
visit_threshold = 1

def plot_classes(axes, move_classes, vals, tp, color, marker):
    mvlist = move_classes[tp][color]
    if len(mvlist)>0:
        x = [j+1 for j in mvlist]
        vals = [vals[i] for i in mvlist]
        axes.scatter(x, vals, color=color, edgecolor='black', marker=marker,
                     label='%s %s' % (color, tp.lower()))
        [axes.annotate(str(j), xy=(j,v),
                       xytext=(j,1.01+0.01*(j%5)if color=='black' else -0.01-0.01*(j%5)),
                       arrowprops={'arrowstyle':'-', 'linestyle':'dashed',
                                   'fc':'grey', 'ec':'grey'},
                       va='top' if color=='black' else 'bottom',
                       ha='center', fontsize=6) for j, v in zip(x,vals)]

def compile_move_evals(moves, evals):
    move_classes = {cls:{'black':[], 'white':[]} for cls in move_classifications}
    vals = []
    for i, (m, v) in enumerate(zip(moves, evals[:-1])):
        color, move = m
        best, analysis = v

        value = analysis[best['chosen']]['V']
        if color=='white': value = 1-value
        vals.append(value)

        move_classes[best['classification']][color].append(i)

    return vals, move_classes

def get_intervals(evals):
    active_intervals = {}
    completed_intervals = []
    for i, (s, _) in enumerate(evals):
        for m in s.get('urgent', set()):
            start,_ = active_intervals.get(m, (i+1,i+1))
            active_intervals[m] = start, i+1

        for m in set(active_intervals.keys()):
            if m not in s.get('urgent', set()):
                completed_intervals.append((active_intervals[m], m))
                del active_intervals[m]
    return sorted(completed_intervals)

def summarize(moves, evals, fn):
    vals, move_classes = compile_move_evals(moves, evals)
    urgent_intervals = get_intervals(evals)

    fig, axes = plt.subplots(1, 1, figsize=(10,8))

    mvrng = list(range(1,len(evals)))
    axes.plot(mvrng, vals, color='k')

    baseline = evals[0][1][evals[0][0]['chosen']]['V']
    axes.plot([0,mvrng[-1]],[baseline]*2,color='grey', linestyle='--',
              label='baseline evaluation')

    for color in ['black', 'white']:
        plot_classes(axes, move_classes, vals, 'EXCELLENT', color, '*')
        plot_classes(axes, move_classes, vals, 'MISTAKE', color, 'o')
        plot_classes(axes, move_classes, vals, 'BLUNDER', color, '^')

    interval_baseline = -0.18
    interval_adjust = 0.06
    mx_end = 0
    current_adjustment = 0
    first = True
    for (start, end), mv in urgent_intervals:
        if start < mx_end:
            current_adjustment+=1
        else:
            current_adjustment=0
        y = interval_baseline + current_adjustment*interval_adjust

        axes.plot([start,end-1], [y,y], linestyle='solid', linewidth=2,
                  marker='s', markersize=4, color='purple', label='urgent' if first else None)
        axes.text( (start+end-1)/2., y+0.01, convert_position(mv),
                   ha='center', va='bottom')
        if end>mx_end:
            mx_end=end
        first=False

    xt = np.arange(0,len(moves),5)
    axes.set_xticks(xt)
    axes.set_xticklabels([("\n%d" if i%2 else "%d") % (i) for i in xt])
    axes.set_xlim(-1,mvrng[-1]+1)
    axes.set_ylim(-0.19,1.1)
    axes.set_yticks(np.arange(0,1.1,0.1))
    axes.set_xlabel("Move", fontsize=12)
    axes.set_ylabel("Win probability (black)", fontsize=12)
    axes.tick_params(labelsize=8)

    plt.legend(loc=3, fontsize=6)

    plt.savefig(fn, dpi=200, bbox_inches='tight')

def convert_position(pos):
    abet = 'abcdefghijklmnopqrstuvwxyz'
    mapped = 'abcdefghjklmnopqrstuvwxyz'
    pos = '%s%d' % (mapped[abet.index(pos[0])], 19-abet.index(pos[1]))
    return pos

def annotate_move(node, i, m, e, e2, classify=True):
    color, move = m
    best, analysis = e
    comment = "Move %d\n" % (i+1)

    urgent_moves = e[0].get('urgent', set()) & e2[0].get('urgent', set())
    candidate_moves = [mv for mv in analysis.keys() if (logit(analysis[mv]['V']) - logit(analysis[best['chosen']]['V']) >= display_cutoff) and \
                                                       (analysis[mv]['visits'] >= display_visits)]

    sorted_moves = sorted(candidate_moves, key=lambda k: -analysis[k]['visits'])
    sorted_moves = [mv for mv in sorted_moves if analysis[mv]['visits']>visit_threshold]
    LB_vals = {}

    for i, mv in enumerate(sorted_moves[:3]):
        lb = chr(ord('A')+i)
        LB_vals[mv] = lb
        if mv in urgent_moves: LB_vals[mv] += '*'

    if classify:
        mv_cls = best.get('classification', None)
        if mv_cls == 'EXCELLENT':
            LB_vals[move] = '!!'
            comment += "Excellent move (!!)\n"
        elif mv_cls == 'GOOD':
            LB_vals[move] = '!'
            comment += "Good move (!)\n"
        elif mv_cls == 'INTERESTING':
            LB_vals[move] = '!?'
            comment += "Interesting move (!?)\n"
        elif mv_cls == 'MISTAKE':
            LB_vals[move] = '?'
            comment += "Mistake (?)\n"
        elif mv_cls == 'BLUNDER':
            LB_vals[move] = '??'
            comment += "Blunder (??)\n"
    #        elif best['classification'] == 'INACCURACY':
    #            LB_vals[move] = '?!'

    if e2 is not None and e2[0]['chosen'] != 'resign':
        wr = winrate(e2)
        if color=='black': wr=1-wr
        comment += "Winrate: %d%%\n" % (int(100*wr))
        if e is not None:
            wr2 = winrate(e)
            if color=='white': wr2=1-wr2
            comment += "Difference: %.1f%% (%.2f)\n" % (100*(wr-wr2), logit(wr) - logit(wr2))
        comment += "Next move: %s\n" % (convert_position(e2[0]['chosen']))
    elif e2[0]['chosen'] != 'resign':
        comment += "Next move: resign"

    for mv in urgent_moves:
        comment += "Urgent: %s%s\n" % (convert_position(mv), " (%s)" % (LB_vals[mv]) if mv in LB_vals else "")

    comment += "\nTop moves:\n"
    for i, mv in enumerate(sorted_moves[:3]):
        move_analysis = analysis[mv]
        wr = move_analysis['V']
        visits = move_analysis['visits']

        lb = LB_vals[mv]
        if color=='white': wr=1-wr
        comment += "%s: %d%% (%d)\n" % (lb, int(100*wr), visits)

    node.addProperty( node.makeProperty( 'LB', ["%s:%s" % (k,v) for k,v in LB_vals.items()] ) )

    if node.has_key('C'):
        node['C'].data[0] = comment
    else:
        node.addProperty( node.makeProperty( 'C', [comment] ) )

def annotate_sequence( node, i, note=None ):
    comment = "Move %d\nPredicted sequence" % (i+1)

    if note is not None:
        comment += "\n%s" % (note)

    if node.has_key('C'):
        node['C'].data[0] = comment
    else:
        node.addProperty( node.makeProperty( 'C', [comment] ) )

def add_sequence(C, i, color, e, sequence_limit, note=None):
    best = e[0]['chosen']
    if best == 'resign': return
    seq = e[1][best]['seq'][:sequence_limit]
    last = seq[-1]
    for mv in seq:
        nnode = sgflib.Node()
        annotate_sequence( nnode, i, note=note if mv==last else None )
        ctag = 'W' if color == 'white' else 'B'
        nnode.addProperty( nnode.makeProperty(ctag, [mv]) )
        C.appendNode( nnode )
        C.next( len(C.children) - 1 )
        color = 'white' if color=='black' else 'black'
        i+=1
    for mv in seq:
        C.previous()

def add_refutation(C, i, color, mv, e, sequence_limit, note=None):
    nnode = sgflib.Node()
    ctag = 'W' if color == 'white' else 'B'
    nnode.addProperty( nnode.makeProperty(ctag, [mv]) )
    if note is not None:
        nnode.addProperty( nnode.makeProperty('C', ["Move %d\nGame mistake\n%s" % (i+1, note)]) )
    C.appendNode(nnode)
    C.next( len(C.children)-1 )
    add_sequence(C, i+1, 'black' if color == 'white' else 'white', e, sequence_limit, note=note)

def add_branches(C, i, color, e, sequence_limit, show_sequences=False):
    branches = e[0].get('branches', [])
    if len(branches)==0 and show_sequences:
        add_sequence(C, i, color, e, sequence_limit)

    for mv, bev in branches:
        nnode = sgflib.Node()
        ctag = 'W' if color == 'white' else 'B'
        nnode.addProperty( nnode.makeProperty(ctag, [mv]) )
        C.appendNode( nnode )
        C.next( len(C.children) - 1 )
        annotate_move(C.node, i, (color, mv), e, bev, classify=False)
        add_branches(C, i+1, 'white' if color=='black' else 'black', bev, sequence_limit, show_sequences=True)
        C.previous()

def add_main_branch(C, i, color, e, sequence_limit, show_sequences=False, note=None):
    branches = e[0].get('branches', [])
    if len(branches)==0 and show_sequences:
        add_sequence(C, i, color, e, sequence_limit, note=note)

    else:
        mv, bev = branches[0]
        nnode = sgflib.Node()
        ctag = 'W' if color == 'white' else 'B'
        nnode.addProperty( nnode.makeProperty(ctag, [mv]) )
        C.appendNode( nnode )
        C.next( len(C.children) - 1 )
        annotate_move(C.node, i, (color, mv), e, bev, classify=False)
        add_main_branch(C, i+1, 'white' if color=='black' else 'black', bev, sequence_limit, show_sequences=True, note=note)
        C.previous()

def assemble(moves, evals, sgf_fn, fn, sequence_limit):
    sgf = gotools.import_sgf(sgf_fn)
    C = sgf.cursor()

    for i, (m, e, e2) in enumerate(zip(moves, evals[:-1], evals[1:])):
        add_branches(C, i, m[0], e, sequence_limit)
        C.next()
        annotate_move(C.node, i, m, e, e2)

    m = moves[i]
    if m[1] not in ['', 'tt']:
        add_sequence(C, i, 'black' if m[0] == 'white' else 'white', evals[i+1], 2*sequence_limit)

    with open(fn, 'w') as ofile:
        print(sgf, file=ofile)

def to_black(rate, turn=None, color=None):
    if turn is not None:
        return rate if turn%2==0 else 1-rate
    elif color is not None:
        return rate if color=='black' else 1-rate

def make_tsumego(moves, evals, sgf_fn, path, sequence_limit):
    if not os.path.exists(path):
        os.makedirs(path)

    moves_used = set()

    sgf = gotools.import_sgf(sgf_fn)
    C = sgf.cursor()
    G = gotools.Goban(sgf)

    problems = 0
    for i, (m, e, e2) in enumerate(zip(moves, evals[:-1], evals[1:])):
        best, analysis = e
        branches = best.get('branches', [])
        mv_best = best['chosen']

        turn = m[0]
        mv_chosen = m[1]
        best_winrate = winrate(e)
        chosen_winrate = 1 - winrate(e2)
        wdiff = chosen_winrate - best_winrate

        mv_cls = best.get('classification', None)
        if mv_cls in ['MISTAKE', 'BLUNDER'] and mv_best not in moves_used and len(branches)>0:
            tsumego_sgf = sgflib.SGFParser(G.to_sgf(comment="%s to play" % (turn.capitalize()))).parse()
            C2 = tsumego_sgf.cursor()

            add_main_branch(C2, i, turn, e, sequence_limit, 
                         note="Gain:    %.1f%%\nBlack winrate: %.1f%%" %
                            (-wdiff*100., to_black(best_winrate, color=turn)*100.))
            add_refutation(C2, i, turn, mv_chosen, e2, sequence_limit,
                         note="Loss:    %.1f%%\nBlack winrate: %.1f%%" %
                           (wdiff*100., to_black(chosen_winrate, color=turn)*100.))
            moves_used.add(mv_best)

            with open(os.path.join(path, "%d.sgf" % (i+1)), 'w') as ofile:
                print(str(tsumego_sgf), file=ofile, end='')
                problems+=1

        C.next()
        G.perform(C.node)

    print("Extracted %d tsumego" % (problems), file=sys.stderr)

def winrate(v, mv=None):
    if mv is None:
        best, analysis = v
        mv = best['chosen']
    return analysis[mv]['V']

def classification(logodds):
    lcut=np.inf
    for cls, cutoff in move_cutoffs:
        if lcut > logodds >= cutoff:
            return cls
        lcut=cutoff

SKIP_THRESHOLD=1
def find_urgent(moves, evals):
    streaks = {}

    for i, (m, v) in enumerate(zip(moves, evals)):
        for k in set(streaks.keys()):
            seen, missed, first = streaks[k]
            streaks[k] = seen, missed+1, first

        best_move = v[0]['chosen']
        seen, _, first = streaks.get(best_move, (0, 0, i))
        streaks[best_move] = seen+1, 0, first

        for k in set(streaks.keys()):
            seen, missed, first = streaks[k]
            if missed>SKIP_THRESHOLD:
                del streaks[k]
            if seen>2:
                for e in evals[first:i]:
                    e[0].setdefault('urgent', set()).add(k)

def classify_moves(moves, evals):
    i=0
    for (color, pos), v, nv in zip(moves, evals, evals[1:]):
        i+=1
        wdiff = 1 - winrate(nv) - winrate(v)
        logodds = logit(1-winrate(nv)) - logit(winrate(v))
        cls = classification(logodds)

        if (cls=='NEUTRAL' or cls=='GOOD') and pos not in v[1]:
            cls = 'INTERESTING'

        summary, _ = v
        summary['classification'] = cls
        summary['diff'] = wdiff

    evals[-1][0]['classification']=None
    evals[-1][0]['diff']=None

def cache_output(func, filename, *args, **kwargs):
    try:
        with open(filename,'rb') as pfile:
            rval = pickle.load(pfile)
    except:
        rval = func(*args, **kwargs)
        with open(filename,'wb') as ofile:
            pickle.dump(rval, ofile)
    return rval

def iter_moves(fn):
    sgf = gotools.import_sgf(fn)

    C = sgf.cursor()
    while not C.atEnd:
        C.next()
        if 'W' in C.node.keys():
            yield 'white', C.node['W'].data[0]
        if 'B' in C.node.keys():
            yield 'black', C.node['B'].data[0]

def get_explore_moves(ev, branch_limit):
    best, analysis = ev
    chosen = best['chosen']
    yield chosen

    sorted_moves = sorted(analysis.keys(), key=lambda k: -analysis[k]['visits'])
    wr = ev[1][chosen]['V']
    pl = ev[1][chosen]['visits']

    i=1
    for mv in sorted_moves:
        if mv==chosen: continue
        if logit(wr) - logit(ev[1][mv]['V']) > exploration_cutoff and\
           ev[1][mv]['visits'] > pl*exploration_visits_fraction:
            yield mv
            i+=1
        if i>branch_limit: break

def explore_branches(move_num, lz, ev, output, max_depth, branch_limit):
    def recursive_explore(mv, ev, node_number, sequence, node_depth=0):
        node_number+=1
        color = lz.whoseturn()
        lz.play_move(color, mv)

        if 'branches' not in ev[0]:
            ev[0]['branches'] = []

        cachefile = os.path.join(output, '%d.%d.%s.pyp' % (move_num, node_depth, sequence))
        ev2 = cache_output(lz.analyze, cachefile)
        ev[0]['branches'].append((mv, ev2))
        if node_depth < max_depth:
            for mv2 in get_explore_moves(ev2, branch_limit):
                node_number = recursive_explore(mv2, ev2, node_number, sequence+mv2, node_depth+1)

        lz.undo()
        return node_number

    node_number = 0
    for mv in get_explore_moves(ev, branch_limit):
        node_number = recursive_explore(mv, ev, node_number, mv)
    print("Explored %d nodes" % (node_number), file=sys.stderr)

@click.command()
@click.option('--executable', default='leelaz')
@click.option('--branch-limit', default=1)
@click.option('--end', default=100000000, type=int)
@click.option('--black', 'analyze_color', flag_value='black')
@click.option('--white', 'analyze_color', flag_value='white')
@click.option('--depth', default=2, type=int)
@click.option('--limit', default=8, type=int)
@click.option('--playouts', default=5000, type=int)
@click.option('--verbose', default=True, is_flag=True)
@click.option('--test', is_flag=True, default=False)
@click.argument('sgf')
@click.argument('output')
def main(executable, branch_limit, end, analyze_color, depth, limit, playouts, verbose, test, sgf, output):
    if not os.path.exists(output):
        os.makedirs(output)
    assert os.path.isdir(output), "Output target is not a directory: %s" % (output)
    moves = list(iter_moves(sgf))
    evals = []

    print("Analyzing game...", file=sys.stderr)
    if test:
        lz = leelaz.DummyCLI()
    else:
        lz = leelaz.CLI(executable=executable, verbosity=3 if verbose else 1, eval_limit=playouts)
    with lz, click.progressbar(moves+[None], show_pos=True, file=sys.stderr) as pb:
        for i, move in enumerate(pb):
            if i > end:
                break
            analysis_file = os.path.join(output, '%d.pyp' % (i))
            evaluation = cache_output(lz.analyze, analysis_file)
            if evaluation[0]['chosen']=='resign':
                break
            evals.append(evaluation)
            logodds = logit(1-winrate(evals[-1]))-logit(winrate(evals[-2])) if len(evals)>2 else 0
            if verbose: print("Move %d, logodds: %.2f" % (i, logodds), file=sys.stderr)

            if logodds < analysis_cutoff and \
               (1-winrate(evals[-2])) > no_explore_winrate and \
               winrate(evals[-2]) > no_explore_winrate:
                last_move = lz.undo()
                if analyze_color is None or last_move[0] == analyze_color:
                    explore_branches(i, lz, evals[-2], output, depth, branch_limit)
                lz.play_move(*last_move)
            if move is not None:
                lz.play_move(*move)

            if verbose:
                print("Finished analyzing move %d" % (i+1), file=sys.stderr)
                lz.boardstate()

    print("Analyzing mistakes...", file=sys.stderr)
    classify_moves(moves, evals)
    find_urgent(moves, evals)
    assemble(moves, evals, sgf, os.path.join(output, 'out.sgf'), limit)
    make_tsumego(moves, evals, sgf, os.path.join(output, 'tsumego'), limit)
    summarize(moves, evals, os.path.join(output, 'summary.pdf'))

if __name__=='__main__':
    main()

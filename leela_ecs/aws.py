from __future__ import print_function
import os
import sys
import threading
import subprocess
import hashlib
import time
import boto3
from leela_ecs import config
from leela_ecs import log

AWS_STATE_DIR = 'aws'
AWS_DEFAULT_CPUS = 1
AWS_DEFAULT_MEM = 4000
AWS_DEFAULT_IMAGE = 'ubuntu'
AWS_SCISCONS_INIT_COMMAND = 'apt-get -y update && apt-get install -y scons mercurial && hg clone https://bitbucket.org/mkmatlock/sciscons /tmp/sciscons && cd /tmp/sciscons && pip --no-cache-dir install --upgrade . && cd $SCISCONS_PROJECT_SCRATCH && sciscons-job'
AWS_GPU_INSTANCE_TYPES = ['g2', 'g3', 'p2', 'p3']

createEnvTemplate="""\
{
    "computeEnvironmentName": "",
    "type": "MANAGED",
    "state": "ENABLED",
    "computeResources": {
        "type": "EC2",
        "minvCpus": 0,
        "maxvCpus": 256,
        "desiredvCpus": 0,
        "instanceTypes": [
            ""
        ],
        "imageId": "",
        "subnets": [
            ""
        ],
        "securityGroupIds": [
            ""
        ],
        "ec2KeyPair": "",
        "instanceRole": "",
        "tags": {
        },
    },
    "serviceRole": ""
}"""

updateEnvTemplate="""\
{
    "computeEnvironment": "",
    "state": "DISABLED"
}"""

createQueueTemplate="""\
{
    "jobQueueName": "",
    "state": "ENABLED",
    "priority": 1,
    "computeEnvironmentOrder": [
        {
            "order": 0,
            "computeEnvironment": ""
        }
    ]
}"""

updateQueueTemplate="""\
{
    "jobQueue": "",
    "state": "ENABLED"
}"""

deleteJobQueueTemplate="""\
{
    "jobQueue": ""
}"""

deleteComputeEnvironmentTemplate="""\
{
    "computeEnvironment": ""
}"""

class AWSResourceNotFound(Exception):
    pass

def is_gpu_enabled_instance(inst_type):
    supertype = inst_type.split('.')[0]
    return supertype in AWS_GPU_INSTANCE_TYPES

def parse_dockerhub(hubaddr):
    if hubaddr.startswith('docker://'):
        hubaddr = hubaddr.strip('docker://')

    parts = hubaddr.split('/')
    if len(parts)==1:
        domain='docker'
    else:
        domain = parts[0]

    parts = parts[1].split(':')
    if len(parts)==1:
        image, tag = parts[0], 'latest'
    else:
        image, tag = parts
    return domain, image, tag



class AWSS3(object):
    def __init__(self, session, bucket='leela-zero'):
        self._client = session.client('s3')
        self._resource = session.resource('s3')
        self._bname = bucket

        self._bucket=None
        for bobj in self._resource.buckets.all():
            if bobj.name==bucket:
                self._bucket = bobj
        if self._bucket is None:
            self._bucket = self._client.create_bucket(Bucket=bucket,
                                                      CreateBucketConfiguration={
                                                          'LocationConstraint':'us-east-1'})

    def delete(self, keys):
        self._bucket.delete_objects(Delete={'Objects':[{'Key':k} for k in keys]})

    def list(self):
        for obj in self._bucket.objects.all():
            yield obj.key

    @property
    def bucket(self):
        return self._bname

    def hashfile(self, fpath):
        with open(fpath,'r') as ffile:
            m = hashlib.sha256()
            m.update(ffile.read().encode())
            return m.hexdigest()

    def send_file(self, fpath, hsh):
        rfn = '%s.sgf' % (hsh)
        self._bucket.upload_file(fpath, rfn)
        return hsh

    def get_file(self, remote, dest):
        self._bucket.download_file(remote, dest)

    def get_result(self, fpath, hsh):
        fpath, _ = fpath.rsplit('.',1)
        self._bucket.download_file('%s.tar.gz' % (hsh), fpath + '.tar.gz')

    def check_available(self, hsh):
        response = self._client.list_objects(Bucket=self._bname, Prefix=hsh)
        for obj in response['Contents']:
            if obj['Key'] == '%s.tar.gz' % (hsh): return True
        return False

def logging_thread(stream, output):
    iterator = iter(stream.readline, "")
    t = threading.currentThread()
    while getattr(t, "run", True):
        try:
            line = next(iterator).decode().strip()
            output.append(line)
        except Exception as e:
            pass
    stream.close()

def record_output(command, **kwargs):
    p = subprocess.Popen( command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, **kwargs )
    stdout_data = []
    stderr_data = []
    stdout_thread = threading.Thread( target=logging_thread, args=(p.stdout, stdout_data) )
    stderr_thread = threading.Thread( target=logging_thread, args=(p.stderr, stderr_data) )
    stdout_thread.start()
    stderr_thread.start()
    rc = p.wait()
    stdout_thread.run=False
    stderr_thread.run=False
    stdout_thread.join()
    stderr_thread.join()
    return rc, '\n'.join(stdout_data), '\n'.join(stderr_data)

class AWSInstance(object):
    CONNECTION_TEST_RETRY_LIMIT=9
    def __init__(self, id=None, keyname=None, ami=None, ip=None, zone=None, itype=None, client=None, ec2=None):
        self._keyname = keyname
        self._ami = ami
        self._ip = ip
        self._id = id
        self._zone = zone
        self._itype = itype
        self._client = client
        self._ec2 = ec2

    @property
    def ip(self):
        if self._ip is None:
            response = self._client.describe_instances(InstanceIds=[self._id])
            inst = response['Reservations'][0]
            self._ip = inst['Instances'][0]['PublicIpAddress']
        return self._ip

    def request_instance(self, *args, **kwargs):
        raise NotImplementedError()

    def _assert_connected(self):
        tries=0
        while 1:
            try:
                tries+=1
                output = self._ec2.command(self.ip, "echo \"Connected.\"", echo=False)
                assert 'Connected.' in output, "Unexpected output: %s" % (output)
                break
            except Exception as e:
                print("Unable to connect: %s, retry..." % (str(e)), file=sys.stderr)
                if tries > AWSInstance.CONNECTION_TEST_RETRY_LIMIT:
                    self.terminate_instance()
                    raise Exception("Could not connect to instance: \"%s\", shutting down..." % (str(e)))

    def wait_for_instance(self):
        response = self._client.describe_instances(InstanceIds=[self._id])
        state = response['Reservations'][0]['Instances'][0]['State']['Name']

        while state in ['starting', 'pending']:
            response = self._client.describe_instances(InstanceIds=[self._id])
            state = response['Reservations'][0]['Instances'][0]['State']['Name']

        assert state=='running', "Unexpected instance state: %s" % (state)
        self._assert_connected()

    def terminate_instance(self):
        self._client.terminate_instances(InstanceIds=[self._id])


class AWSOnDemandInstance(AWSInstance):
    def request_instance(self):
        request = {'KeyName': self._keyname,
                 'SecurityGroupIds': ['sg-502d5b1b'],
                 'ImageId': self._ami,
                 'InstanceType': self._itype, 
                 'IamInstanceProfile':{'Name': "ECservices"},
                 'MinCount':1, 'MaxCount':1,
                 'InstanceInitiatedShutdownBehavior': 'terminate'}
        if self._zone:
            request['AvailabilityZone'] = self._zone
        response = self._client.run_instances(**request)
        self._id = response['Instances'][0]['InstanceId']

class AWSSpotInstance(AWSInstance):
    def __init__(self, spot_price=None, request_id=None, **kwargs):
        super(AWSSpotInstance, self).__init__(**kwargs)
        self._spot_price = spot_price
        self._request_id = request_id

    def request_instance(self, inst_type='p2.xlarge'):
        lspec = {'KeyName': self._keyname,
                 'SecurityGroupIds': ['sg-502d5b1b'],
                 'ImageId': self._ami,
                 'InstanceType': self._itype, 
                 'IamInstanceProfile':{'Name': "ECservices"}}
        request = {'LaunchSpecification':lspec, 
                   'SpotPrice': str(self._spot_price), 
                   'Type': 'one-time'}
        if self._zone:
            request['AvailabilityZone'] = self._zone
        response = self._client.request_spot_instances(**request)
        self._request_id = response['SpotInstanceRequests'][0]['SpotInstanceRequestId']
        self.wait_for_request()

    def wait_for_request(self):
        response = self._client.describe_spot_instance_requests(SpotInstanceRequestIds=[self._request_id])
        request = response['SpotInstanceRequests'][0]
        code = request['Status']['Code']

        while code != 'fulfilled':
            if not code.startswith('pending-'):
                self.cancel_request()
                raise Exception("Fulfillment failed with status code: %s" % (code))
            time.sleep(1)
            response = self._client.describe_spot_instance_requests(SpotInstanceRequestIds=[self._request_id])
            request = response['SpotInstanceRequests'][0]
            code = request['Status']['Code']

        self._id = request['InstanceId']

    def cancel_request(self):
        self._client.cancel_spot_instance_requests(SpotInstanceRequestIds=[self._request_id])

class AWSEC2(object):
    def __init__(self, session, key='default', ami='ami-0b98d7f73c7d1bb71', spot_price=None, zone=None):
        self._client = session.client('ec2')
        self._ami = ami
        self._keyname = key
        self._spot_price = spot_price
        self._zone = zone

    def get_connection_string(self, address):
        return ' '.join(['-i', '~/.ssh/%s.pem' % (self._keyname), 
                                              '-o', 'ServerAliveInterval=120',
                                              '-o', 'StrictHostKeyChecking=no',
                                              'ubuntu@%s' % (address)])

    def put_file(self, address, local, rdir, rname):
        self.command(address, "mkdir -p %s" % (rdir))
        rval, out, err  = record_output(['rsync', '-e', 'ssh -i ~/.ssh/%s.pem' % (self._keyname), 
                                         local, 'ubuntu@%s:%s/%s' % (address, rdir, rname)])
        if rval != 0:
            raise Exception("Called process exception, code %d\nSTDOUT:\n%s\nSTDERR:\n%s" % (rval, out, err))


    def command(self, address, cmd, echo=True):
        if echo:
            print("Run: %s" % (cmd), file=sys.stderr)
        rval, out, err  = record_output(['ssh', '-i', '~/.ssh/%s.pem' % (self._keyname), 
                                              '-o', 'ServerAliveInterval=120',
                                              '-o', 'StrictHostKeyChecking=no',
                                              'ubuntu@%s' % (address), cmd])
        if rval!=0:
            raise Exception("Called process exception, code %d\nSTDOUT:\n%s\nSTDERR:\n%s" % (rval, out, err))
        return out

    def request_instance(self, instance_type='p2.xlarge'):
        if self._spot_price is None:
            inst = AWSOnDemandInstance(keyname=self._keyname, ami=self._ami,
                                       zone=self._zone, client=self._client,
                                       itype=instance_type, ec2=self)
        else:
            inst = AWSSpotInstance(spot_price=self._spot_price, keyname=self._keyname,
                                   ami=self._ami, client=self._client,
                                   itype=instance_type, ec2=self)
        inst.request_instance()
        return inst

        # 'VpcId': "vpc-8248aaf8",
#            lspec['MaxCount']=1
#            lspec['MinCount']=1
#            instances = self._client.create_instances(**lspec)
#            return instances[0]

    def get_instance(self):
        ami_filter={'Name': 'image-id', 'Values': [self._ami]}
        response = self._client.describe_instances(Filters=[ami_filter])
        for inst in response['Reservations']:
            if inst['Instances'][0]['State']['Name'] in ['running', 'starting', 'pending']:
                return AWSInstance(id=inst['Instances'][0]['InstanceId'],
                                   client=self._client,
                                   ec2=self)

class AWSBatch(object):
    STATUS_KEYS = ['SUBMITTED', 'PENDING', 'RUNNABLE', 'STARTING', 'RUNNING', 'SUCCEEDED', 'FAILED']

    def __init__(self):
        self._client = boto3.client('batch')
        self._logs = boto3.client('logs')

        self._config = config.load_configuration()
        self._compute_environments = self._describe_cenvs()
        self._job_queues = self._describe_job_queues()
        self._job_defs = self._describe_job_defs()

    @property
    def client(self):
        return self._client

    @property
    def job_queues(self):
        return dict(self._job_queues.items())

    @property
    def job_definitions(self):
        return dict(self._job_defs.items())

    @property
    def compute_environments(self):
        return dict(self._compute_environments.items())

    def _get_aws_credential_vars( ):
        with open(os.path.expanduser('~/.aws/credentials'), 'r') as credfile:
            cvars = {}
            for line in credfile:
                try:
                    k,v = line.split('=')
                    cvars[k.strip().upper()] = v.strip()
                except:
                    pass
            return cvars

    def _jobs_status(self, aws_job_ids):
        results = self._client.describe_jobs( jobs=aws_job_ids )
        return {st:sum(1 for job in results['jobs'] if job['status']==st) for st in self.STATUS_KEYS}

    def _describe_jobs(self, aws_job_ids):
        return {job['jobId']:job for job in self._client.describe_jobs( jobs=aws_job_ids )['jobs']}

    def _wait_for_jobs(self, aws_job_ids):
        logger = log.get()
        while 1:
            status = self._jobs_status(aws_job_ids)
            if all(status[st]==0 for st in self.STATUS_KEYS if st not in {'FAILED', 'SUCCEEDED'}):
                break
            logger.info( 'Waiting on jobs:   %s', '    '.join('%5d %s' % (status[st], st) for st in self.STATUS_KEYS) )
            time.sleep(1)

        logger.info( 'Jobs completed: %5d SUCCEEDED  %5d FAILED', status['SUCCEEDED'], status['FAILED'])

    def _get_job_log(self, aws_job_status):
        log_stream = aws_job_status['container']['logStreamName']
        return self._logs.get_log_events( logGroupName='/aws/batch/job',
                                          logStreamName=log_stream )

    def _create_job(self, job_name, job_queue_arn, job_def_arn, command, env, profile):
        return self._client.submit_job(
                            jobName=job_name,
                            jobQueue=job_queue_arn,
                            jobDefinition=job_def_arn,
                            containerOverrides={'command': command,
                                                'vcpus': int(profile.get('cpus', AWS_DEFAULT_CPUS)),
                                                'memory': int(profile.get('memory', AWS_DEFAULT_MEM)),
                                                'environment': [{'name':k, 'value':v} for k, v in env.items()]
                                                })

    def _describe_job_defs(self):
        job_definitions = {}

        description = self._client.describe_job_definitions()
        for jobdef in description['jobDefinitions']:
            name = jobdef['jobDefinitionName']
            status = jobdef['status']
            if name.startswith('sciscons') and status == 'ACTIVE':
                arn = jobdef['jobDefinitionArn']
                image = parse_dockerhub( jobdef['containerProperties']['image'] )
                job_definitions[image] = (name, arn)

        return job_definitions

    def _select_or_create_job_definition(self, profile):
        image = profile.get('image', AWS_DEFAULT_IMAGE)
        image_key = parse_dockerhub( image )

        if not image_key in self._job_defs:
            defname = ('leela-%s-%s-%s-job' % image_key).replace('.', '-')
            self._job_defs[image_key] = self._create_job_definition(defname, image)

        return self._job_defs[image_key]

    def _delete_job_definition(self, job_def_name):
        for name, arn in self._job_defs.values():
            if name == job_def_name:
                self._client.deregister_job_definition( jobDefinition=arn )
                log.get().info( "Deleted job definition: %s" % (name) )

    def _create_job_definition(self, job_def_name, image, default_cpus=AWS_DEFAULT_CPUS, default_memory=AWS_DEFAULT_MEM):
        response = self._client.register_job_definition(jobDefinitionName=job_def_name,
                                                        type='container',
                                                        containerProperties={
                                                            'image': image,
                                                            'vcpus': default_cpus,
                                                            'memory': default_memory,
                                                            'privileged': True,
                                                            'volumes': [
                                                                {
                                                                    'host': {
                                                                        'sourcePath': '/var/lib/nvidia-docker/volumes/nvidia_driver/latest'
                                                                    },
                                                                    'name': 'nvidia'
                                                                },
                                                                {
                                                                    'host': {
                                                                        'sourcePath': '/scratch'
                                                                    },
                                                                    'name': 'scratch'
                                                                }
                                                            ],
                                                            'mountPoints': [
                                                                {
                                                                    'containerPath': '/usr/local/nvidia',
                                                                    'readOnly': True,
                                                                    'sourceVolume': 'nvidia'
                                                                },
                                                                {
                                                                    'containerPath': '/scratch',
                                                                    'readOnly': False,
                                                                    'sourceVolume': 'scratch'
                                                                }
                                                            ]
                                                        })
        log.get().info( 'Created AWS batch job definition %s' % response['jobDefinitionName'] )
        return response['jobDefinitionName'], response['jobDefinitionArn']

    def _describe_job_queues(self):
        logger = log.get()
        queues = {}
        description = self._client.describe_job_queues()
        for jq in description['jobQueues']:
            queue_name = jq['jobQueueName']
            if queue_name.startswith('sciscons'):
                arn = jq['jobQueueArn']
                resources = [ cenv['computeEnvironment'] for cenv in jq['computeEnvironmentOrder'] ]
                queues[queue_name] = (arn, resources)
                logger.debug('Found queue %s with resources: %s' % (arn, ' '.join(resources)))
        return queues

    def _select_job_queue(self, profile):
        logger = log.get()
        gpus = int(profile.get('gpus', 0))
        cpus = int(profile.get('cpus', AWS_DEFAULT_CPUS))
        inst_type = profile.get('instance_type', None)
#        memory = profile.get('memory', AWS_DEFAULT_MEM)

        for jq in self._job_queues:
            arn, cenv_list = self._job_queues[jq]
            for cenv_name in cenv_list:
                cenv = self._compute_environments[cenv_name]
                if gpus > 0 and all( not is_gpu_enabled_instance(inst) for inst in cenv['instance_types'] ):
                    continue

                if inst_type is not None and \
                    inst_type not in cenv['instance_types'] and \
                    inst_type.split('.')[0] not in cenv['instance_types'] and \
                    inst_type not in [it.split('.')[0] for it in cenv['instance_types']]:
                    continue

                if cpus < cenv['min_cpu'] or cpus > cenv['max_cpu']:
                    continue
                return jq, arn

        raise AWSResourceNotFound("No job queue was found for the resource request: %s" % (repr(profile)))

    def _create_job_queue(self, queue_name, compute_env, priority=0):
        response = self._client.create_job_queue(jobQueueName=queue_name,
                                        priority=priority,
                                        computeEnvironmentOrder=[
                                            {
                                                'order': 0,
                                                'computeEnvironment': compute_env
                                            }
                                        ])
        log.get().info( 'Created AWS queue: %s' % (response['jobQueueName']) )
        return response['jobQueueArn']

    def _describe_cenvs(self):
        logger = log.get()
        cenvs = {}
        description = self._client.describe_compute_environments()
        for cenv in description['computeEnvironments']:
            arn = cenv['computeEnvironmentArn']
            cenvs[arn] = {'min_cpu': cenv['computeResources']['minvCpus'],
                          'max_cpu': cenv['computeResources']['maxvCpus'],
                          'ami': cenv['computeResources']['imageId'],
                          'instance_types': [str(it) for it in cenv['computeResources']['instanceTypes']]}
            logger.debug("Found compute environment %s, configuration: %s" % (arn, str(cenvs[arn])))
        return cenvs

    def submit_jobs(self, jobs):
        job_submissions = []
        for job in jobs:
            job_name = self._get_job_name( job.sconsfile )
            _, jq_arn = self._select_job_queue( job.resources )
            _, jd_arn = self._select_or_create_job_definition( job.resources )

            job_submissions.append(( job_name, jq_arn, jd_arn, job.sconsfile, job.resources ))

        aws_jobs = []
        for job_name, jq_arn, jd_arn, sconsfile, profile in job_submissions:
            env = {'LEELA_BUCKET': self.bucket}
            aws_job = self._create_job( job_name, jq_arn, jd_arn, ['bash', '-c', AWS_SCISCONS_INIT_COMMAND], env, profile )
            aws_jobs.append( aws_job )
        return aws_jobs

    def stop_jobs(self, aws_job_ids):
        for jobId in aws_job_ids:
            self._client.terminate_job(jobId=jobId, reason="SciSCons build canceled.")

    def setup_aws(self, name, config):
        logger = log.get()

        cmd = eval(createEnvTemplate)
        cmd['computeEnvironmentName'] = name
        for k in config:
            if k == 'service-role':
                cmd['serviceRole'] = config[k]
            if k == 'instance-types':
                cmd['computeResources']['instanceTypes'] = config[k]
            if k == 'security-groups':
                cmd['computeResources']['securityGroupIds'] = config[k]
            if k == 'instance-role':
                cmd['computeResources']['instanceRole'] = config[k]
            if k == 'keypair':
                cmd['computeResources']['ec2KeyPair'] = config[k]
            if k == 'image':
                cmd['computeResources']['imageId'] = config[k]
            if k == 'subnets':
                cmd['computeResources']['subnets'] = config[k]

        logger.info("Creating cenv: %s" % (cmd))
        self.client.create_compute_environment(**cmd)
        desc = self.client.describe_compute_environments(computeEnvironments=[name])
        while desc['computeEnvironments'][0]['status']!='VALID':
            logger.info("Waiting on compute environment, status: %s" % (desc['computeEnvironments'][0]['status']))
            desc = self.client.describe_compute_environments(computeEnvironments=[name])
            time.sleep(1)

        cmd = eval(createQueueTemplate)
        jqname = "%s-queue" % (name)
        cmd['jobQueueName'] = jqname
        cmd['computeEnvironmentOrder'][0]['computeEnvironment'] = name
        logger.info("Creating job queue: %s" % (jqname))
        self.client.create_job_queue(**cmd)
        desc = self.client.describe_job_queues(jobQueueName=[jqname])
        while desc['jobQueues'][0]['status']!='VALID':
            logger.info("Waiting on job queue, status: %s" % (desc['jobQueues'][0]['status']))
            desc = self.client.describe_job_queues(jobQueueName=[jqname])
            time.sleep(1)

    def reset_aws(self, name):
        logger = log.get()

        for jq in self.job_queues:
            if jq.startswith('sciscons'):
                logger.info("Disabling: %s" % (jq))
                first=True
                while 1:
                    desc = self.client.describe_job_queues(jobQueues=[jq])
                    if desc['jobQueues'][0]['state'] == 'DISABLED':
                        break
                    if first:
                        cmd = eval(updateQueueTemplate)
                        cmd['jobQueue'] = jq
                        cmd['state'] = 'DISABLED'
                        self.client.update_job_queue(**cmd)
                        first=False
                    time.sleep(1)

                if desc['jobQueues'][0]['status']!='DELETING' and \
                    desc['jobQueues'][0]['status']!='DELETED':
                    logger.info("Deleting: %s" % (jq))
                    cmd = eval(deleteJobQueueTemplate)
                    cmd['jobQueue'] = jq
                    self.client.delete_job_queue(**cmd)
                while desc['jobQueues'][0]['status']!='DELETED':
                    logger.info("Deleting: %s" % (jq))
                    desc = self.client.describe_job_queues(jobQueues=[jq])
                    time.sleep(1)


        for ce in self.compute_environments:
            arn, name = ce.rsplit('/', 1)
            if name.startswith('sciscons'):
                logger.info("Disabling: %s" % (name))
                first=True
                while 1:
                    desc = self.client.describe_compute_environments(computeEnvironments=[ce])
                    if desc['computeEnvironments'][0]['state'] == 'DISABLED':
                        break
                    if first:
                        cmd = eval(updateEnvTemplate)
                        cmd['computeEnvironment'] = ce
                        cmd['state'] = "DISABLED"
                        self.client.update_compute_environment(**cmd)
                        first=False
                    time.sleep(1)

                if desc['computeEnvironments'][0]['status']!='DELETING' and \
                    desc['computeEnvironments'][0]['status']!='DELETED':
                    logger.info("Deleting: %s" % (name))
                    cmd = eval(deleteComputeEnvironmentTemplate)
                    cmd['computeEnvironment'] = ce
                    self.client.delete_compute_environment(**cmd)
                while desc['computeEnvironments'][0]['status']!='DELETED':
                    logger.info("Deleting: %s" % (name))
                    desc = self.client.describe_compute_environments(computeEnvironments=[ce])
                    time.sleep(1)

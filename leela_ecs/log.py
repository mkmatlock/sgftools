import logging

__log=None
def get(to_file='leela-ecs.log'):
    global __log
    if __log is None:
        __log = logging.getLogger('sciscons')
        __log.setLevel( logging.DEBUG )
        __log.handlers = []
        ch = logging.StreamHandler()
        ch.setLevel( logging.INFO )
        formatter = logging.Formatter('%(levelname)s - %(message)s')
        ch.setFormatter( formatter )
        __log.addHandler( ch )

        fh = logging.FileHandler( to_file )
        fh.setLevel( logging.DEBUG )
        formatter = logging.Formatter('%(asctime)s %(levelname)s - %(message)s')
        fh.setFormatter( formatter )
        __log.addHandler( fh )
    return __log
